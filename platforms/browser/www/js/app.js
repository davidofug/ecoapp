var	upload_in_progress = false,
    subcounties_uploaded = false,
    villages_uploaded = false,
    producer_groups_uploaded = false,
    producers_uploaded = false,
    areas_uploaded = false,
    next_year = 0,
    plants_uploaded = false,
    monitoring_uploaded = false,
    appName = 'EcoApp',
    appVersion = '0.0.2',
    app = new Framework7({
        modalTitle: appName,
        material: true,
        animatePages: true
    }),
    $$ = Dom7,
    mainView = app.addView( '.view-main', {
        domCache: true,
        dynamicNavbar: true
    }),
    setupStatusInterval = null,
    downloadPagination = {
        page: 1,
        per_page: 15
    },
    uploadPagination = {
        page: 1,
        per_page: 15,
    },
    db = window.openDatabase( 'com.ecotrust', appVersion, appName, 1000000 ),
    currentUser = {
        id: null,
        remote_id: null,
        name: {
            first: null,
            last: null,
            user: null
        },
        districts: null
    },
    apiBase = 'http://ggrids.com/ecotrust/api/',
    district = null,
    subCounty = null,
    group = null;

$$( '.app-name' ).text( appName );

function setSetupStatus( status ) {
    $$( '#setup-loader' ).css( {
        'margin-top' : parseInt( ( $$( window ).height() - 100 ) / 2 ) + 'px'
    });

    removeSetupStatus();

    app.popup( '#setup-screen', false, true );

    $$( '#setup-status span' ).text( status );

    setupStatusInterval = setInterval( function() {
        var container = $$( '#setup-status em' ),
            length = container.text().length;

        if ( length == 0 )
            container.text( '.' );

        else if ( length == 1 )
            container.text( '..' );

        else if ( length == 2 )
            container.text( '...' )

        else
            container.text( '' );
    }, 500 );
}

function removeSetupStatus() {
    if ( !setupStatusInterval == null || typeof setupStatusInterval != 'undefined' )
        clearInterval( setupStatusInterval );
}

function registerInstallation() {
    setSetupStatus( 'Registering installation' );

    app.popup( '#setup-screen', false, true );

    $$.post(
        apiBase + 'inbound',
        {
            method: 'register_installation',
            version: appVersion,
            device_info: JSON.stringify( device )
        },
        function( response ) {
            var response = JSON.parse( response );

            setTimeout( function() {
                if ( response.result == 'success' ) {
                    window.localStorage.setItem( 'token', response.data.token );
                    window.localStorage.setItem( 'id', response.data.id );

                    window.localStorage.setItem( 'installationRegistered', 1 );

                    downloadData( 'countries' );
                }

                else {
                    app.alert( 'Please connect to the internet !' ,'Error');

                    setTimeout( function() {
                        navigator.app.exit();
                    }, 3000 );
                }
            }, 3000 );
        },
        function() {
            app.alert( 'Please connect to the internet !', 'Error' );

            setTimeout( function() {
                navigator.app.exit();
            }, 3000 );
        }
    );
}

function downloadData( entries ) {
    switch ( entries ) {
        case 'countries' :

            if ( downloadPagination.page == 1 )
                truncateTable( 'countries' );

            setSetupStatus( 'Downloading countries' );

            $$.post(
                apiBase + 'outbound',
                {
                    method: 'get_countries',
                    page: downloadPagination.page,
                    per_page: downloadPagination.per_page,
                    token: window.localStorage.getItem( 'token' )
                },
                function( response ) {
                    var response = JSON.parse( response );

                    if ( response.result == 'success' ) {
                        if ( response.countries.length > 0 )
                            db.transaction( function( tx ) {
                                $$.each( response.countries, function( index, country ) {
                                    tx.executeSql( 'INSERT INTO countries ( remote_id, code, name ,  created_at  , updated_at) VALUES ( ?, ?, ?, ?, ? )', [ country.id, country.code, country.name , get_current_timestamp() ,get_current_timestamp() ] );
                                });
                            }, null );

                        downloadPagination.page = response.countries < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

                        if ( downloadPagination.page > 1 )
                            downloadData( 'countries' );

                        else
                            downloadData( 'districts' );
                    }

                    else
                        downloadData( 'countries' );
                },
                function() {
                    download_data( 'countries' );
                }
            );

            break;

        case 'districts' :

            if ( downloadPagination.page == 1 )
                truncateTable( 'districts' );

            setSetupStatus( 'Downloading districts' );

            $$.post(
                apiBase + 'outbound',
                {
                    method: 'get_districts',
                    page: downloadPagination.page,
                    per_page: downloadPagination.per_page,
                    token: window.localStorage.getItem( 'token' )
                },
                function( response ) {
                    var response = JSON.parse( response );

                    if ( response.result == 'success' ) {
                        if ( response.districts.length > 0 )
                            db.transaction( function( tx ) {
                                $$.each( response.districts, function( index, district ) {
                                    tx.executeSql( 'INSERT INTO districts ( remote_id, country_id, name ,  created_at  , updated_at ) VALUES ( ?, ( SELECT id FROM countries WHERE remote_id = ? ) , ? , ? , ? )', [ district.id, district.location.country, district.name  , get_current_timestamp() , get_current_timestamp() ] );
                                });
                            }, null );

                        downloadPagination.page = response.districts < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

                        if ( downloadPagination.page > 1 )
                            downloadData( 'districts' );

                        else
                            downloadData( 'technicians' );
                    }

                    else
                        downloadData( 'districts' );
                },
                function() {
                    downloadData( 'districts' );
                }
            );

            break;

        case 'technicians' :

            if ( downloadPagination.page == 1 )
                truncateTable( 'technicians' );

            setSetupStatus( 'Downloading technicians' );

            $$.post(
                apiBase + 'outbound',
                {
                    method: 'get_technicians',
                    page: downloadPagination.page,
                    per_page: downloadPagination.per_page,
                    token: window.localStorage.getItem( 'token' )
                },
                function( response ) {
                    var response = JSON.parse( response );

                    if ( response.result == 'success' ) {
                        if ( response.technicians.length > 0 )
                            db.transaction( function( tx ) {
                                $$.each( response.technicians, function( index, technician ) {
                                    if ( technician.app_access.allowed ) {
                                        var districtIds;

                                        if ( technician.location.districts.length == 0 )
                                            districtIds = JSON.stringify( [] );

                                        else {
                                            tx.executeSql(
                                                'SELECT id FROM districts WHERE remote_id IN ( ' + technician.location.districts.join( ', ' ) + ' ) ',
                                                [],
                                                function( tx, result ) {
                                                    var ids = [];

                                                    for ( i = 0; i < result.rows.length; i++ )
                                                        ids.push( result.rows.item( i ).id );

                                                    districtIds = JSON.stringify( ids );
                                                },
                                                function( tx, error ) {
                                                    districtIds = JSON.stringify( [] );
                                                }
                                            );
                                        }

                                        var delayInsertRecord = window.setInterval( function() {
                                            if ( districtIds != null ) {
                                                db.transaction( function( tx ) {
                                                    tx.executeSql(
                                                        'INSERT INTO technicians ( remote_id, first_name, last_name, user_name, district_ids, password  , created_at  , updated_at) VALUES ( ?, ?, ?, ? , ?, ?, ?, ? )',
                                                        [ technician.id, technician.name.first, technician.name.last, technician.name.user, districtIds, technician.app_access.password , get_current_timestamp() ,get_current_timestamp() ]
                                                    );
                                                }, null );

                                                clearInterval( delayInsertRecord );
                                            }
                                        }, 50 );
                                    }
                                });
                            }, null );

                        downloadPagination.page = response.technicians < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

                        if ( downloadPagination.page > 1 )
                            downloadData( 'technicians' );

                        else {
                            window.localStorage.setItem( 'setOneSetup', 1 );

                            openLoginScreen();

                            setTimeout( function() {
                                app.closeModal( '#setup-screen', true );
                            }, 2000 );
                        }
                    }

                    else
                        downloadData( 'technicians' );
                },
                function() {
                    downloadData( 'technicians' );
                }
            );

            break;

        case 'sub-counties' :

            if ( downloadPagination.page == 1 )
                truncateTable( 'sub_counties' );

            setSetupStatus( 'Downloading sub-counties' );

            db.transaction( function( tx ) {
                tx.executeSql( 'SELECT remote_id FROM districts WHERE id IN ( ' + currentUser.districts.join( ', ' ) + ' )', [], function( tx, result ) {
                    var districts = [];

                    for ( i = 0; i < result.rows.length; i++ )
                        districts.push( result.rows.item( i ).remote_id );

                    $$.post(
                        apiBase + 'outbound',
                        {
                            method: 'get_sub_counties',
                            page: downloadPagination.page,
                            per_page: downloadPagination.per_page,
                            token: window.localStorage.getItem( 'token' ),
                            account: currentUser.remote_id,
                            districts: districts
                        },
                        function( response ) {
                            var response = JSON.parse( response );

                            if ( response.result == 'success' ) {
                                if ( response.sub_counties.length > 0 )
                                    db.transaction( function( tx ) {
                                        $$.each( response.sub_counties, function( index, sub_county ) {
                                            tx.executeSql( 'INSERT INTO sub_counties ( remote_id, district_id, name ,  created_at  , updated_at ) VALUES ( ?, ( SELECT id FROM districts WHERE remote_id = ? ), ? , ? , ? )', [ sub_county.id, sub_county.location.district, sub_county.name  , get_current_timestamp() , get_current_timestamp()] );
                                        });
                                    }, null );

                                downloadPagination.page = response.sub_counties < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

                                if ( downloadPagination.page > 1 )
                                    downloadData( 'sub-counties' );

                                else
                                    downloadData( 'villages' );
                            }

                            else
                                downloadData( 'sub-counties' );
                        },
                        function() {
                            downloadData( 'sub-counties' );
                        }
                    );
                }, function( tx, error ) {
                    downloadData( 'sub-counties' );
                });
            });

            break;

        case 'villages' :

            if ( downloadPagination.page == 1 )
                truncateTable( 'villages' );

            setSetupStatus( 'Downloading villages' );

            db.transaction( function( tx ) {
                tx.executeSql( 'SELECT remote_id FROM sub_counties', [], function( tx, result ) {
                    var sub_counties = [];

                    for ( i = 0; i < result.rows.length; i++ )
                        sub_counties.push( result.rows.item( i ).remote_id );

                    $$.post(
                        apiBase + 'outbound',
                        {
                            method: 'get_villages',
                            page: downloadPagination.page,
                            per_page: downloadPagination.per_page,
                            token: window.localStorage.getItem( 'token' ),
                            account: currentUser.remote_id,
                            sub_counties: sub_counties
                        },
                        function( response ) {
                            var response = JSON.parse( response );

                            if ( response.result == 'success' ) {
                                if ( response.villages.length > 0 )
                                    db.transaction( function( tx ) {
                                        $$.each( response.villages, function( index, village ) {
                                            tx.executeSql( 'INSERT INTO villages ( remote_id, sub_county_id, name , created_at  , updated_at ) VALUES ( ?, ( SELECT id FROM sub_counties WHERE remote_id = ? ), ? , ? , ? )', [ village.id, village.location.sub_county, village.name ,get_current_timestamp() , get_current_timestamp() ] );
                                        });
                                    }, null );

                                downloadPagination.page = response.villages < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

                                if ( downloadPagination.page > 1 )
                                    downloadData( 'villages' );

                                else
                                    downloadData( 'banks' );
                            }

                            else
                                downloadData( 'villages' );
                        },
                        function() {
                            downloadData( 'villages' );
                        }
                    );
                }, function( tx, error ) {
                    downloadData( 'villages' );
                });
            });

            break;

        case 'banks' :

            if ( downloadPagination.page == 1 )
                truncateTable( 'banks' );

            setSetupStatus( 'Downloading banks' );

            $$.post(
                apiBase + 'outbound',
                {
                    method: 'get_banks',
                    page: downloadPagination.page,
                    per_page: downloadPagination.per_page,
                    token: window.localStorage.getItem( 'token' ),
                    account: currentUser.remote_id
                },
                function( response ) {
                    var response = JSON.parse( response );

                    if ( response.result == 'success' ) {
                        if ( response.banks.length > 0 )
                            db.transaction( function( tx ) {
                                $$.each( response.banks, function( index, bank ) {
                                    tx.executeSql( 'INSERT INTO banks ( remote_id, name , created_at  , updated_at ) VALUES ( ?, ? , ?, ? )', [ bank.id, bank.name ,get_current_timestamp(),get_current_timestamp() ] );
                                });
                            }, null );

                        downloadPagination.page = response.banks < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

                        if ( downloadPagination.page > 1 )
                            downloadData( 'banks' );

                        else
                            downloadData( 'bank-branches' );
                    }

                    else
                        downloadData( 'banks' );
                },
                function() {
                    downloadData( 'banks' );
                }
            );

            break;

        case 'bank-branches' :

            if ( downloadPagination.page == 1 )
                truncateTable( 'bank_branches' );

            setSetupStatus( 'Downloading bank branches' );

            $$.post(
                apiBase + 'outbound',
                {
                    method: 'get_bank_branches',
                    page: downloadPagination.page,
                    per_page: downloadPagination.per_page,
                    token: window.localStorage.getItem( 'token' ),
                    account: currentUser.remote_id
                },
                function( response ) {
                    var response = JSON.parse( response );

                    if ( response.result == 'success' ) {
                        if ( response.bank_branches.length > 0 )
                            db.transaction( function( tx ) {
                                $$.each( response.bank_branches, function( index, bank_branch ) {
                                    tx.executeSql( 'INSERT INTO bank_branches ( remote_id, bank_id, name ,  created_at  , updated_at ) VALUES ( ?, ( SELECT id FROM banks WHERE remote_id = ? ), ? ,? ,? )', [ bank_branch.id, bank_branch.bank, bank_branch.name  , get_current_timestamp()  , get_current_timestamp()] );
                                });
                            }, null );

                        downloadPagination.page = response.bank_branches < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

                        if ( downloadPagination.page > 1 )
                            downloadData( 'bank-branches' );

                        else
                            downloadData( 'land-systems' );
                    }

                    else
                        downloadData( 'bank-branches' );
                },
                function() {
                    downloadData( 'bank-branches' );
                }
            );

            break;

        case 'land-systems' :

            if ( downloadPagination.page == 1 )
                truncateTable( 'land_systems' );

            setSetupStatus( 'Downloading land systems' );

            $$.post(
                apiBase + 'outbound',
                {
                    method: 'get_land_systems',
                    page: downloadPagination.page,
                    per_page: downloadPagination.per_page,
                    token: window.localStorage.getItem( 'token' ),
                    account: currentUser.remote_id
                },
                function( response ) {
                    var response = JSON.parse( response );

                    if ( response.result == 'success' ) {
                        if ( response.land_systems.length > 0 )
                            db.transaction( function( tx ) {
                                $$.each( response.land_systems, function( index, land_system ) {
                                    tx.executeSql( 'INSERT INTO land_systems ( remote_id, name ,  created_at  , updated_at) VALUES ( ?, ? ,?, ? )', [ land_system.id, land_system.name , get_current_timestamp(), get_current_timestamp() ] );
                                });
                            }, null );

                        downloadPagination.page = response.land_systems < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

                        if ( downloadPagination.page > 1 )
                            downloadData( 'land-systems' );

                        else
                            downloadData( 'producer-groups' );
                    }

                    else
                        downloadData( 'land-systems' );
                },
                function() {
                    downloadData( 'land-systems' );
                }
            );

            break;


        case 'producer-groups' :

            if ( downloadPagination.page == 1 )
                truncateTable( 'producer_groups' );

            setSetupStatus( 'Downloading producer groups' );

            db.transaction( function( tx ) {
                tx.executeSql( 'SELECT remote_id FROM villages', [], function( tx, result ) {
                    var villages = [];

                    for ( i = 0; i < result.rows.length; i++ )
                        villages.push( result.rows.item( i ).remote_id );

                    $$.post(
                        apiBase + 'outbound',
                        {
                            method: 'get_producer_groups',
                            page: downloadPagination.page,
                            per_page: downloadPagination.per_page,
                            token: window.localStorage.getItem( 'token' ),
                            account: currentUser.remote_id,
                            villages: villages
                        },
                        function( response ) {
                            var response = JSON.parse( response );

                            if ( response.result == 'success' ) {
                                if ( response.producer_groups.length > 0 )
                                    db.transaction( function( tx ) {
                                        $$.each( response.producer_groups, function( index, producer_group ) {
                                            tx.executeSql( 'INSERT INTO producer_groups ( remote_id, village_id, name ,  created_at  , updated_at  ) VALUES ( ?, ( SELECT id FROM villages WHERE remote_id = ? ), ? , ? ,? )', [ producer_group.id, producer_group.location.village, producer_group.name  ,get_current_timestamp() ,get_current_timestamp() ] );
                                        });
                                    }, null );

                                downloadPagination.page = response.producer_groups.length < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

                                if ( downloadPagination.page > 1 )
                                    downloadData( 'producer-groups' );

                                else
                                    downloadData( 'producers' );
                            }

                            else
                                downloadData( 'producer-groups' );
                        },
                        function() {
                            downloadData( 'producer-groups' );
                        }
                    );
                }, function( tx, error ) {
                    downloadData( 'producer-groups' );
                });
            });

            break;


        case 'producers' :

            if ( downloadPagination.page == 1 )
                truncateTable( 'producers' );

            setSetupStatus( 'Downloading producers' );

            db.transaction( function( tx ) {
                tx.executeSql( 'SELECT remote_id FROM villages', [], function( tx, result ) {
                    var villages = [];

                    for ( i = 0; i < result.rows.length; i++ )
                        villages.push( result.rows.item( i ).remote_id );

                    $$.post(
                        apiBase + 'outbound',
                        {
                            method: 'get_producers',
                            page: downloadPagination.page,
                            per_page: downloadPagination.per_page,
                            token: window.localStorage.getItem( 'token' ),
                            account: currentUser.remote_id,
                            villages: villages
                        },
                        function( response ) {
                            var response = JSON.parse( response );

                            if ( response.result == 'success' ) {

                                if ( response.producers.length > 0 )
                                    db.transaction( function( tx ) {
                                        $$.each( response.producers, function( index, producer ) {

                                            var  waitingInterval = setInterval( function() {

                                                    db.transaction( function( tx ) {
                                                        tx.executeSql(
                                                            'INSERT INTO producers ( remote_id, village_id, code, first_name, other_names, gender, date_of_birth, phone_number, photo, producer_groups,  bank_details , created_at , updated_at ) VALUES ( ?, ( SELECT id FROM villages WHERE remote_id = ? ), ?, ?,  ?, ?, ?, ?, ?, ?, ?, ?, ? )',
                                                            [ producer.id, producer.location.village, producer.code, producer.name.first, producer.name.others, producer.gender, producer.dob, producer.phone, producer.photo, JSON.stringify( producer.groups ),  JSON.stringify( producer.banking) ,get_current_timestamp(), get_current_timestamp() ]
                                                        );
                                                    } );

                                                    clearInterval( waitingInterval );
                                            } );
                                        } );
                                    }, null );

                                downloadPagination.page = response.producers.length < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

                                if ( downloadPagination.page > 1 )
                                    downloadData( 'producers' );

                                else
                                    downloadData( 'tree-species' );

                            }

                            else
                                downloadData( 'producers' );
                        },
                        function() {
                            downloadData( 'producers' );
                        }
                    );
                }, function( tx, error ) {
                    downloadData( 'producer-groups' );
                } );
            } );

            break;

        case 'tree-species' :

            if ( downloadPagination.page == 1 )
                truncateTable( 'tree-species' );

            setSetupStatus( 'Downloading tree species' );

            $$.post(
                apiBase + 'outbound',
                {
                    method: 'get_tree_species',
                    page: downloadPagination.page,
                    per_page: downloadPagination.per_page,
                    token: window.localStorage.getItem( 'token' ),
                    account: currentUser.remote_id
                },
                function( response ) {
                    var response = JSON.parse( response );

                    if ( response.result == 'success' ) {
                        if ( response.tree_species.length > 0 )
                            db.transaction( function( tx ) {
                                $$.each( response.tree_species, function( index, tree_specie ) {
                                    tx.executeSql( 'INSERT INTO tree_species ( remote_id,  name ,  created_at  , updated_at ) VALUES ( ? , ? ,? ,? )', [ tree_specie.id, tree_specie.name  , get_current_timestamp()  , get_current_timestamp()] );
                                });
                            }, null );

                        downloadPagination.page = response.tree_species < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

                        if ( downloadPagination.page > 1 )
                            downloadData( 'tree-species' );

                        else

                            downloadData( 'causes' );
                    }
                    else
                        downloadData( 'tree-species' );
                },
                function() {
                    downloadData( 'tree-species' );
                }
            );
            break;

        case 'causes' :

            if ( downloadPagination.page == 1 )
                truncateTable( 'causes' );

            setSetupStatus( 'Downloading tree death causes' );

            $$.post(
                apiBase + 'outbound',
                {
                    method: 'get_tree_death_causes',
                    page: downloadPagination.page,
                    per_page: downloadPagination.per_page,
                    token: window.localStorage.getItem( 'token' ),
                    account: currentUser.remote_id
                },
                function( response ) {
                    var response = JSON.parse( response );

                    if ( response.result == 'success' ) {
                        if ( response.tree_death_causes.length > 0 )
                            db.transaction( function( tx ) {
                                $$.each( response.tree_death_causes, function( index, cause ) {
                                    tx.executeSql( 'INSERT INTO causes ( remote_id,  name ,  created_at  , updated_at ) VALUES ( ? , ? ,? ,? )', [ cause.id , cause.name , get_current_timestamp()  , get_current_timestamp()] );
                                });
                            }, null );

                        downloadPagination.page = response.tree_death_causes < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

                        if ( downloadPagination.page > 1 )
                            downloadData( 'causes' );

                        else
                            downloadData( 'corrective-actions' )
                    }
                    else
                        downloadData( 'cause' );
                },
                function() {
                    downloadData( 'cause' );
                }
            );
            break;

        case 'corrective-actions' :

            if ( downloadPagination.page == 1 )
                truncateTable( 'corrective-actions' );

            setSetupStatus( 'Downloading corrective actions' );

            $$.post(
                apiBase + 'outbound',
                {
                    method: 'get_corrective_actions',
                    page: downloadPagination.page,
                    per_page: downloadPagination.per_page,
                    token: window.localStorage.getItem( 'token' ),
                    account: currentUser.remote_id
                },
                function( response ) {
                    var response = JSON.parse( response );

                    if ( response.result == 'success' ) {
                        if ( response.corrective_actions.length > 0 )
                            db.transaction( function( tx ) {
                                $$.each( response.corrective_actions, function( index, corrective_action ) {
                                    tx.executeSql( 'INSERT INTO corrective_actions ( remote_id,  name ,  created_at  , updated_at ) VALUES ( ? , ? ,? ,? )', [ corrective_action.id , corrective_action.name , get_current_timestamp()  , get_current_timestamp()] );
                                });
                            }, null );

                        downloadPagination.page = response.corrective_actions < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

                        if ( downloadPagination.page > 1 )
                            downloadData( 'corrective-actions' );

                        else
                            downloadData( 'areas' )

                    }
                    else
                        downloadData( 'corrective-actions' );
                },
                function() {
                    downloadData( 'corrective-actions' );
                }
            );
            break;

        case 'areas' :

            if ( downloadPagination.page == 1 )
                truncateTable( 'areas' );

            setSetupStatus( 'Downloading areas' );

            $$.post(
                apiBase + 'outbound',
                {
                    method: 'get_areas',
                    page: downloadPagination.page,
                    per_page: downloadPagination.per_page,
                    token: window.localStorage.getItem( 'token' ),
                    account: currentUser.remote_id,
                    districts: currentUser.district
                },
                function( response ) {
                    var response = JSON.parse( response );

                    if ( response.result == 'success' ) {
                        if ( response.areas.length > 0 )
                            db.transaction( function( tx ) {
                                $$.each( response.areas, function( index, area ) {
                                    tx.executeSql( 'INSERT INTO areas ( remote_id, code , producer_id, land_system  , tgb_area_size , gps , land_Size , village_id , created_at  , updated_at  ) VALUES ( ?,  ?, ( SELECT id FROM producers WHERE remote_id = ? ) , ( SELECT id FROM land_systems WHERE remote_id = ? ), ?, ?, ?,( SELECT id FROM villages WHERE remote_id = ? ) , ? ,? )',
                                        [ area.id, area.code, area.producer, area.land_system, area.area_size ,area.gps , area.land_size , area.village ,get_current_timestamp() ,get_current_timestamp() ] );
                                });
                            }, null );

                        downloadPagination.page = response.areas.length < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

                        if ( downloadPagination.page > 1 )
                            downloadData( 'areas' );

                        else
                            downloadData( 'plantings' );

                    }
                    else
                        downloadData( 'areas' );
                },
                function() {
                    downloadData( 'areas' );
                }
            );

            break;

        case 'plantings' :

            if ( downloadPagination.page == 1 )
                truncateTable( 'plantings' );

            setSetupStatus( 'Downloading plantings' );

            $$.post(
                apiBase + 'outbound',
                {
                    method: 'get_planting',
                    page: downloadPagination.page,
                    per_page: downloadPagination.per_page,
                    token: window.localStorage.getItem( 'token' ),
                    account: currentUser.remote_id,
                    districts: currentUser.district
                },
                function( response ) {
                    var response = JSON.parse( response );

                    if ( response.result == 'success' ) {
                        if ( response.plantings.length > 0 )
                            db.transaction( function( tx ) {
                                $$.each( response.plantings, function( index, planting ) {
                                    tx.executeSql( 'INSERT INTO plantings ( remote_id, code , producer_id, area_id , year , planting_started , tree_target , spacing , planted_species , created_at  , updated_at  ) VALUES ( ?, ? , ( SELECT id FROM producers WHERE remote_id = ? ) , ( SELECT id FROM areas WHERE remote_id = ? ), ? , ?, ?, ? , ? , ? , ? )',
                                        [ planting.id, planting.code ,  planting.producer, planting.area , planting.year, planting.planting_started , planting.tree_target ,planting.spacing , JSON.stringify( planting.tree_species ) ,get_current_timestamp() ,get_current_timestamp() ] );
                                });
                            }, null );

                        downloadPagination.page = response.plantings.length < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

                        if ( downloadPagination.page > 1 )
                            downloadData( 'plantings' );

                        else
                            downloadData( 'monitorings' );

                    }
                    else
                        downloadData( 'plantings' );
                },
                function() {
                    downloadData( 'plantings' );
                }
            );

            break;

        case 'monitorings' :

            if ( downloadPagination.page == 1 )
                truncateTable( 'monitorings' );

            setSetupStatus( 'Downloading monitorings' );

            $$.post(
                apiBase + 'outbound',
                {
                    method: 'get_monitoring',
                    page: downloadPagination.page,
                    per_page: downloadPagination.per_page,
                    token: window.localStorage.getItem( 'token' ),
                    account: currentUser.remote_id
                },
                function( response ) {
                    var response = JSON.parse( response );

                    if ( response.result == 'success' ) {
                        if ( response.monitoring.length > 0 )
                            db.transaction( function( tx ) {
                                $$.each( response.monitoring, function( index, monitor ) {
                                    tx.executeSql( 'INSERT INTO monitorings (  remote_id , code , producer_id ,  area_id  , year  , tree_target  , correct_area  ,correct_tree_species  , main_corrective_action  , causes_of_trees_dying ,date_taken , seedling_source  , number_of_seeds  ,   notes  , qualify  , tree_samples  , created_at  , updated_at  )  VALUES ( ?, ? ,( SELECT id FROM producers WHERE remote_id = ? ) , ( SELECT id FROM areas WHERE remote_id = ? ), ? , ?, ?, ? , ( SELECT id FROM corrective_actions WHERE remote_id = ? ) , ( SELECT id FROM causes WHERE remote_id = ? ) , ? , ? , ? , ? ,? ,? , ? , ? )',
                                        [ monitor.id, monitor.code ,  monitor.producer, monitor.area , monitor.year , monitor.tree_target_met ,monitor.correct_areas_planted , monitor.correct_tree_species_planted , monitor.main_corrective_action , monitor.causes_of_trees_dying , monitor.corrective_action_date , monitor.source_of_seeds , monitor.number_of_seeds , monitor.comments , monitor.producer_qualify , JSON.stringify( monitor.samples_of_plots )  , get_current_timestamp() ,get_current_timestamp() ] );

                                });
                            }, null );

                        downloadPagination.page = response.monitoring.length < downloadPagination.per_page ? 1 : ( downloadPagination.page + 1 );

                        if ( downloadPagination.page > 1 )
                            downloadData( 'monitorings' );

                        else{
                            window.localStorage.setItem( 'technicianSetup', 1 );

                            setTimeout( function() {
                                app.closeModal( '#setup-screen', true );
                            }, 2000 );
                        }
                    }
                    else
                        downloadData( 'monitorings' );
                },
                function() {
                    downloadData( 'monitorings' );
                }
            );

            break;

    }
}

function createDatabaseTables() {
    db.transaction( function( tx ) {
        var tables = [ 'settings', 'countries', 'districts', 'technicians', 'banks', 'bank_branches', 'tech_specifications', 'tech_specification_descriptions', 'producer_groups', 'producers' ];

        for ( index = 0; index < tables.length; index++ )
            tx.executeSql( 'DROP TABLE IF EXISTS ' + tables[ index ] );

        tx.executeSql( 'CREATE TABLE settings ( id INTEGER PRIMARY KEY AUTOINCREMENT, name CHAR(250), value TEXT )' );
        tx.executeSql( 'CREATE TABLE countries ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, code CHAR(3), name CHAR(160), created_at  CHAR(60) , updated_at CHAR(60) )' );
        tx.executeSql( 'CREATE TABLE districts ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, country_id INTEGER, name CHAR(160) , created_at  CHAR(60) , updated_at CHAR(60))' );
        tx.executeSql( 'CREATE TABLE sub_counties ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, district_id INTEGER, name CHAR(160) , created_at  CHAR(60) , updated_at CHAR(60) )' );
        tx.executeSql( 'CREATE TABLE villages ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, sub_county_id INTEGER, name CHAR(160), created_at  CHAR(60) , updated_at CHAR(60) )' );
        tx.executeSql( 'CREATE TABLE technicians ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, first_name CHAR(80), last_name CHAR(80), user_name CHAR(160), district_ids CHAR(15), password CHAR(160), created_at  CHAR(60) , updated_at CHAR(60) )' );
        tx.executeSql( 'CREATE TABLE banks ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, name CHAR(160), created_at  CHAR(60) , updated_at CHAR(60) )' );
        tx.executeSql( 'CREATE TABLE bank_branches ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, bank_id INTEGER, name CHAR(160), created_at  CHAR(60) , updated_at CHAR(60) )' );
        tx.executeSql( 'CREATE TABLE land_systems ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, name CHAR(160), created_at  CHAR(60) , updated_at CHAR(60) )' );
        tx.executeSql( 'CREATE TABLE producer_groups ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, village_id INTEGER, name CHAR(160) , created_at  CHAR(60) , updated_at CHAR(60))' );
        tx.executeSql( 'CREATE TABLE producers ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, village_id INTEGER, code CHAR(20),pv_number CHAR(20), first_name CHAR(80), other_names CHAR(80), gender INTEGER, date_of_birth CHAR(10), phone_number CHAR(12), photo TEXT, producer_groups CHAR(160) NULL , bank_details TEXT , created_at  CHAR(60) , updated_at CHAR(60) )' );
        tx.executeSql( 'CREATE TABLE tree_species ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, name  CHAR(20), created_at  CHAR(60) , updated_at CHAR(60) )' );
        tx.executeSql( 'CREATE TABLE corrective_actions ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, name  CHAR(20), created_at  CHAR(60) , updated_at CHAR(60) )' );
        tx.executeSql( 'CREATE TABLE causes ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, name  CHAR(20), created_at  CHAR(60) , updated_at CHAR(60) )' );
        tx.executeSql( 'CREATE TABLE areas ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, code CHAR(60) , producer_id  INTEGER, land_system TINYINT , tgb_area_size TINYINT NOT NULL , gps CHAR(60) , land_Size TINYINT NOT NULL ,village_id INTEGER , created_at  CHAR(60) , updated_at CHAR(60) )' );
        tx.executeSql( 'CREATE TABLE plantings ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL , code CHAR(60) , producer_id  INTEGER, area_id  INTEGER,  year  TINYINT , planting_started TINYINT , tree_target INTEGER, spacing TINYINT , planted_species TEXT ,created_at  CHAR(60) , updated_at CHAR(60) )' );
        tx.executeSql( 'CREATE TABLE monitorings ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, code CHAR(60), producer_id INTEGER,  area_id  INTEGER, year TINYINT   , tree_target  CHAR(10) , correct_area  CHAR(10) ,correct_tree_species  CHAR(10) , main_corrective_action  TINYINT , causes_of_trees_dying  TINYINT, date_taken  CHAR(60) , seedling_source  CHAR(60), number_of_seeds CHAR(60) ,   notes TEXT , qualify CHAR(10) , tree_samples TEXT  , created_at  CHAR(60) , updated_at CHAR(60) )' );
        tx.executeSql( 'CREATE TABLE social_economics ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, producer_id  INTEGER, year TINYINT NOT NULL , crops_grown CHAR(200) , crop_yield_before CHAR(100) , current_crop_yield CHAR(100) ,other_enterprises CHAR(60) , fund_source  CHAR(60), carbon_income  CHAR(60) , belong_to_group  TINYINT , village_group_name CHAR(60) ,   attend_training TINYINT , training_organiser CHAR(60) , training_venue  CHAR(60)  , training_aim CHAR(60) ,income_crease TINYINT ,income_increase_amount CHAR(60) , has_improved_stove TINYINT, stove_provider CHAR(60) , created_at  CHAR(60) , updated_at CHAR(60) )' );
        tx.executeSql( 'CREATE TABLE planted_species ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, planting_id  INTEGER, specie_id INTEGER , number INTEGER, created_at  CHAR(60) , updated_at CHAR(60) )' );
        tx.executeSql( 'CREATE TABLE sampled_trees ( id INTEGER PRIMARY KEY AUTOINCREMENT, remote_id INTEGER NULL, monitoring_id  INTEGER, height CHAR(60) , crown CHAR(60), tree_growth CHAR(10) , created_at  CHAR(60) , updated_at CHAR(60) )' );

    }, null );
}

function get_current_timestamp() {
    var $full_date = new Date();

    var $month = ( $full_date.getMonth() + 1 ) + '',
        $date = $full_date.getDate() + '',
        $hours = $full_date.getHours() + '',
        $minutes = $full_date.getMinutes() + '',
        $seconds = $full_date.getSeconds() + '';

    return $full_date.getFullYear() + '-' + ( $month.length == 1 ? '0' + $month : $month ) + '-' + ( $date.length == 1 ? '0' + $date : $date ) + ' ' +
        ( $hours.length == 1 ? '0' + $hours : $hours ) + ':' + ( $minutes.length == 1 ? '0' + $minutes : $minutes ) + ':' + ( $seconds.length == 1 ? '0' + $seconds : $seconds );
}

function getRecordCount( table ) {
    db.transaction( function( tx ) {
        tx.executeSql( 'SELECT * FROM ' + table, [], function( tx, result ) {
            for ( i = 0; i < result.rows.length; i++ )
                console.log( result.rows.item( i ) );
        });
    });
}

function truncateTable( table ) {
    db.transaction( function( tx ) {
        tx.executeSql( 'DELETE FROM ' + table );
    });
}

function openLoginScreen() {
    $$( '#login-screen form' ).css( {
        'margin-top' : parseInt( ( $$( window ).height() - 250 ) / 2 ) + 'px'
    });

    $$( '.login-screen-content form [name="username"], .login-screen-content form [name="password"]' ).val( '' );

    $$( '.login-screen-content form .list-button' ).off( 'click' ).on( 'click', function( event ) {
        event.preventDefault();

        var username = $$( '.login-screen-content form [name="username"]' ),
            password = $$( '.login-screen-content form [name="password"]' );

        if ( username.val().trim().length == 0 )
            username.focus();

        else if ( password.val().trim().length == 0 )
            password.focus();

        else {
            app.showPreloader();

            db.transaction( function( tx ) {
                tx.executeSql(
                    'SELECT * FROM technicians WHERE user_name = ? LIMIT 1',
                    [ username.val() ],
                    function( tx, result ) {
                        if ( result.rows.length > 0 ) {
                            tx.executeSql(
                                'SELECT * FROM technicians WHERE user_name = ? AND password = ? LIMIT 1',
                                [ username.val(), password.val() ],
                                function( tx, result ) {
                                    app.hidePreloader();

                                    if ( result.rows.length > 0 ) {
                                        let record = result.rows.item( 0 );

                                        currentUser.id = record.id;
                                        currentUser.remote_id = record.remote_id;
                                        currentUser.name.first = record.first_name;
                                        currentUser.name.last = record.last_name;
                                        currentUser.name.user = record.user_name;
                                        currentUser.districts = JSON.parse( record.district_ids );

                                        window.localStorage.setItem( 'currentUser', JSON.stringify( currentUser ) );


                                        if ( window.localStorage.getItem( 'technicianSetup' ) == null )
                                            setupTechnician();

                                        setTimeout( function() {
                                            app.closeModal( '#login-screen', true );
                                            weedTechnicians();
                                        }, 2000 );
                                    }

                                    else {
                                        app.hidePreloader();

                                        app.alert( 'Wrong password entered. Try again!' );
                                    }
                                },
                                null
                            );
                        }

                        else {
                            app.hidePreloader();

                            app.alert( 'No technician found matching that username. Try again!' );
                        }
                    },
                    null
                );
            });
        }
    });

    app.loginScreen( '#login-screen', true );
}

function setupTechnician() {
    if ( window.localStorage.getItem( 'installationMapped' ) == null ) {
        setSetupStatus( 'Mapping installation' );

        $$.post(
            apiBase + 'inbound',
            {
                method: 'map_installation',
                id: window.localStorage.getItem( 'id' ),
                token: window.localStorage.getItem( 'token' ),
                user: currentUser.remote_id
            },
            function( response ) {
                var response = JSON.parse( response );

                setTimeout( function() {
                    if ( response.result == 'success' ) {
                        window.localStorage.setItem( 'installationMapped', 1 );

                        downloadData( 'sub-counties' );
                    }

                    else {
                        app.alert( 'Please connect to the internet !', 'Error' );

                        setTimeout( function() {
                            navigator.app.exit();
                        }, 3000 );
                    }
                }, 3000 );
            },
            function() {
                app.alert( ' Please connect to the internet !' ,'Error');

                setTimeout( function() {
                    navigator.app.exit();
                }, 3000 );
            });
    }

    else
        downloadData( 'sub-counties' );
}

function weedTechnicians() {
    if ( localStorage.getItem( 'currentUser' ) != null ) {
        let currentUser = JSON.parse( localStorage.getItem( 'currentUser' ) );

        db.transaction( function ( tx ) {
            tx.executeSql( 'DELETE FROM Technicians WHERE id != ? ', [ currentUser.id ], function( tx, reseult ) {	});
        });
    }
}

function setYear( table='monitorings', area ) {
    let years = [ 0, 1, 3, 5, 7, 10];

    var promise = new Promise( function ( resolve, reject ) {
        db.transaction( function(tx) {
            tx.executeSql( `SELECT year FROM ${table} WHERE area_id = ? ORDER BY id DESC LIMIT 1`, [ area ], function (tx, result ) {

                if ( result.rows.length == 1 ) {

                    let record = result.rows.item( 0 );
                    years.indexOf( record.year ) > -1 ? resolve( years[ years.indexOf( record.year ) + 1 ] ) : resolve( parseInt(0) );

                } else
                    reject( 'Next Year not found!' );

            });
        });
    });

    return promise;

}

function loadDistricts() {
    db.transaction( function( tx ) {
        tx.executeSql( 'SELECT id, name FROM districts WHERE id IN ( ' + currentUser.districts.join( ', ' ) + ' ) ORDER BY name ASC', [], function( tx, result ) {
            let markup = ``;

            for ( let i = 0; i < result.rows.length; i++ ) {
                let record = result.rows.item( i );

                markup += `<li>
							<a href="pages/sub-counties.html" data-district-id="${record.id}" class="item-link item-content">
								<div class="item-inner">
									<div class="item-title">${record.name}</div>
								</div>
							</a>
						</li>`;
            }

            $$( '.districts-list-block ul' ).html( markup );

            $$( '.districts-list-block ul li a' ).on( 'click', function( event ) {
                district = $$( this ).attr( 'data-district-id' );
            });
        });
    });
}

$$( document ).on( 'page:init', '.page[data-page="districts"]', function( event ) {
    loadDistricts();
});

function loadSubCounties() {
    db.transaction( function( tx ) {
        tx.executeSql( 'SELECT name FROM districts WHERE id = ? LIMIT 1', [ district ], function( tx, result ) {
            if ( result.rows.length == 1 )
                $$( '.district-name' ).text( result.rows.item( 0 ).name );

        });

        tx.executeSql( 'SELECT id, name FROM sub_counties WHERE district_id = ? ORDER BY name ASC', [ district ], function( tx, result ) {
            let markup = ``;

            for ( i = 0; i < result.rows.length; i++ ) {
                let record = result.rows.item( i );

                markup += `<li>
					<a href="pages/villages.html" data-sub-county-id="${record.id}" class="item-link item-content">
						<div class="item-inner">
							<div class="item-title">${record.name}</div>
						</div>
					</a>
				</li>`;
            }

            $$( '.sub-counties-list-block ul' ).html( markup );

            $$( '.sub-counties-list-block ul li a' ).on( 'click', function( event ) {
                subCounty = $$( this ).attr( 'data-sub-county-id' );
            });
        });
    });
}

$$( document ).on( 'page:init', '.page[data-page="sub-counties"]', function( event ) {
    loadSubCounties();

    $$( '.add-sub-county' ).off( 'click' ).on( 'click', function( event ) {
        app.prompt( 'Enter the sub-county name', 'Add sub-county', function ( name ) {
            if ( name.trim().length > 0 ) {
                db.transaction( function( tx ) {
                    tx.executeSql( 'SELECT id FROM sub_counties WHERE district_id = ? AND name = ?', [ district, name ], function( tx, result ) {
                        if ( result.rows.length == 0 ) {
                            tx.executeSql( 'INSERT INTO sub_counties ( district_id, name , created_at  , updated_at ) VALUES ( ?, ? , ?, ? )', [ district, name , get_current_timestamp() , get_current_timestamp()  ], function( tx, result ) {
                                loadSubCounties();
                            });
                        }

                        else
                            app.alert( 'Sub-county already exists', 'Error' );
                    });
                });
            }

            else
                app.alert( 'Sub-county name provided not valid. Try again!', 'Error' );

        });
    });
});

function loadVillages() {
    db.transaction( function( tx ) {
        tx.executeSql( 'SELECT name FROM sub_counties WHERE id = ? LIMIT 1', [ subCounty ], function( tx, result ) {
            if ( result.rows.length == 1 )
                $$( '.sub-county-name' ).text( result.rows.item( 0 ).name );

        });

        tx.executeSql( 'SELECT id, name FROM villages WHERE sub_county_id = ? ORDER BY name ASC', [ subCounty ], function( tx, result ) {
            let markup = ``;

            for ( let i = 0; i < result.rows.length; i++ ) {
                let record = result.rows.item( i );

                markup += `<li>
					<div class="item-content">
						<div class="item-inner">
							<div class="item-title">${record.name}</div>
						</div>
					</div>
				</li>`;
            }

            $$( '.villages-list-block ul' ).html( markup );
        });
    });
}

$$( document ).on( 'page:init', '.page[data-page="villages"]', function( event ) {
    loadVillages();

    $$( '.add-village' ).off( 'click' ).on( 'click', function( event ) {
        app.prompt( 'Enter the village name', 'Add village', function ( name ) {
            if ( name.trim().length > 0 ) {
                db.transaction( function( tx ) {
                    tx.executeSql( 'SELECT id FROM villages WHERE sub_county_id = ? AND name = ?', [ subCounty, name ], function( tx, result ) {
                        if ( result.rows.length == 0 ) {
                            tx.executeSql( 'INSERT INTO villages ( sub_county_id, name ,  created_at  , updated_at ) VALUES ( ?, ? , ?, ? )', [ subCounty, name  ,get_current_timestamp() ,get_current_timestamp() ], function( tx, result ) {
                                loadVillages();
                                uploadData( 'villages' );
                            });
                        }

                        else
                            app.alert( 'Village already exists', 'Error' );
                    });
                });
            }

            else {
                app.alert( 'Village name provided not valid. Try again!', 'Error' );
            }
        });
    });
});

function loadGroups() {
    db.transaction( function( tx ) {
        tx.executeSql( 'SELECT id, name FROM producer_groups ORDER BY name ASC', [], function( tx, result ) {
            let markup = ``;

            for ( let i = 0; i < result.rows.length; i++ ) {
                let record = result.rows.item( i );

                markup += `<li>
							<a href="pages/group.html" data-group-id="${record.id}" class="item-link item-content">
								<div class="item-inner">
									<div class="item-title">${record.name}</div>
								</div>
							</a>
						</li>`;
            }

            $$( '.groups-list-block ul' ).html( markup );

            $$( '.groups-list-block ul li a' ).on( 'click', function( event ) {
                group = $$( this ).attr( 'data-group-id' );
            });
        });
    });
}

$$( document ).on( 'page:init', '.page[data-page="groups"]', function( event ) {
    loadGroups();

    $$( '.close-add-group-screen' ).off( 'click' ).on( 'click', function( event ) {
        app.closeModal( '#add-group-screen', true );
    });

    $$( '.add-group' ).off( 'click' ).on( 'click', function( event ) {
        app.popup( '#add-group-screen', false, true );
    });
});

$$( document ).on( 'page:init', '.page[data-page="add-group"]', function( event ) {
    db.transaction( function( tx ) {
        tx.executeSql( 'SELECT id, name FROM districts WHERE id IN ( ' + currentUser.districts.join( ', ' ) + ' )', [], function( tx, result ) {
            let markup = `<option value="none">- Select -</option>`;

            for ( let i = 0; i < result.rows.length; i++ ) {
                let record = result.rows.item( i );

                markup += `<option value="${record.id}">${record.name}</option>`;
            }

            $$( '#group-district' ).html( markup ).trigger( 'change' );
        });
    });

    $$( '#group-name' ).on( 'blur', function( event ) {
        if ( $$( this ).val().trim().length == 0 )
            $$( this ).focus();
    }).focus();

    $$( '#group-district' ).off( 'change' ).on( 'change', function( event ) {
        var districtId = $$( this ).val();

        db.transaction( function( tx ) {
            tx.executeSql( 'SELECT id, name FROM sub_counties WHERE district_id = ?', [ districtId ], function( tx, result ) {
                if ( result.rows.length > 0 ) {
                    let markup = ``;

                    for ( i = 0; i < result.rows.length; i++ ) {
                        let record = result.rows.item( i );

                        markup += `<option value="${record.id}">${record.name}</option>`;
                    }

                    $$( '#group-sub-county' ).html( markup ).removeAttr( 'disabled' ).trigger( 'change' );
                }
            });
        });
    });

    $$( '#group-sub-county' ).off( 'change' ).on( 'change', function( event ) {
        let subCountyId = $$( this ).val();

        if ( subCountyId != '' ) {
            db.transaction( function( tx ) {
                tx.executeSql( 'SELECT id, name FROM villages WHERE sub_county_id = ?', [ subCountyId ], function( tx, result ) {
                    if ( result.rows.length > 0 ) {
                        let markup = ``;

                        for ( let i = 0; i < result.rows.length; i++ ) {
                            let record = result.rows.item( i );

                            markup += `<option value="${record.id}">${record.name}</option>`;
                        }

                        $$( '#group-village' ).html( markup ).removeAttr( 'disabled' ).trigger( 'change' );
                    }
                    else
                    {
                        let emptyOption = `<option value="none"> No villages </option>`;
                        $$( '#group-village' ).html( emptyOption ).removeAttr( 'disabled' ).trigger( 'change' );
                    }
                });
            });
        }
    });

    $$( '#save-group' ).off( 'click' ).on( 'click', function( event ) {

        let name = $$( '#group-name' ), district = $$( '#group-district' ), subCounty = $$( '#group-sub-county' ), village = $$( '#group-village' );

        if ( name.val().trim().length === 0 ) {
            name.focus();
            name.focus().css({'border-bottom':'2px #ff2800 solid'});
        }
        else if ( village.val() === '' ) {
            village.focus();
            village.focus().css({'border-bottom':'2px #ff2800 solid'});
        }
        else {
            db.transaction( function( tx ) {
                tx.executeSql( 'SELECT id FROM producer_groups WHERE village_id = ? AND name = ?', [ parseInt( village.val() ), name.val() ], function( tx, result ) {
                    if ( result.rows.length == 0 ) {
                        tx.executeSql( 'INSERT INTO producer_groups ( village_id, name , created_at , updated_at ) VALUES ( ?, ? ,?, ? )', [ parseInt( village.val() ), name.val() ,get_current_timestamp() , get_current_timestamp() ], function( tx, result ) {
                            $$( '.back' ).click();

                            loadGroups();
                            uploadData( 'producer_groups' );
                        });
                    }

                    else {
                        app.alert( 'Producer group already exists. Try again!', 'Error' );

                        name.focus();
                    }
                });
            });
        }
    });
});

function loadGroup() {
    db.transaction(function (tx) {
        tx.executeSql('SELECT * FROM producer_groups WHERE id = ? LIMIT 1', [group], function (tx, result) {
            if (result.rows.length == 1) {
                $$('.group-name').text(result.rows.item(0).name);

                id = result.rows.item(0).remote_id;
                tx.executeSql("SELECT id, code, first_name, other_names, photo FROM producers WHERE producer_groups LIKE '%\"group\":" + id + ",%' ORDER BY first_name ASC", [], function (tx, result) {
                    let markup = ``;

                    for (i = 0; i < result.rows.length; i++) {
                        let record = result.rows.item(i);

                        markup += `<li>
							<a href="pages/producer.html" data-producer-id="${record.id}" class="item-link item-content">
								<div class="item-media">
									<img src="data:image/jpeg;base64,${record.photo}" />
								</div>
								<div class="item-inner">
									<div class="item-title-row">
										<div class="item-title">
											${record.code}
										</div>
									</div>
									<div class="item-subtitle">
										${record.first_name} ${record.other_names}
									</div>
								</div>
							</a>
						</li>`;
                    }

                    $$('.producers-list-block ul').html(markup);

                    $$('.producers-list-block ul li a').on('click', function (event) {
                        producer = $$(this).attr('data-producer-id');
                    });
                });
            }
        });
    });
}


$$( document ).on( 'page:init', '.page[data-page="group"]', function( event ) {
    loadGroup();
});

function loadProducers() {
    db.transaction( function( tx ) {
        tx.executeSql( "SELECT id, code, first_name, other_names, photo FROM producers ORDER BY first_name ASC", [], function( tx, result ) {
            let markup = ``;

            for ( let i = 0; i < result.rows.length; i++ ) {
                let record = result.rows.item( i );

                markup += `<li>
						<a href="pages/producer.html" data-producer-id="${record.id}" class="item-link item-content">
						<div class="item-media">
							<img src="data:image/jpeg;base64,${record.photo}" />
						</div>
						<div class="item-inner">
							<div class="item-title-row">
								<div class="item-title">
									${record.first_name} ${record.other_names}
								</div>
							</div>
							<div class="item-subtitle">

							</div>
						</div>
					</a>
				</li>`;
            }

            $$( '.producers-list-block ul' ).html( markup );

            $$( '.producers-list-block ul li a' ).on( 'click', function( event ) {
                producer = $$( this ).attr( 'data-producer-id' );
            });
        });
    });
}

$$( document ).on( 'page:init', '.page[data-page="producers"]', function( event ) {
    loadProducers();
});

$$( document ).on( 'page:init', '.page[data-page="add-producer"]', function( event ) {

    let calendarDefault = app.calendar({
        input: '#d_o_b ',
        dateFormat: 'MM dd, yyyy'
    });

    db.transaction( function( tx ) {
        tx.executeSql( 'SELECT * FROM banks', [], function( tx, result ) {
            let markup = `<option value="none"> - Select - </option>`;

            for ( let i = 0; i < result.rows.length; i++ ) {
                let  record = result.rows.item( i );

                markup += `<option value="${record.remote_id}">${record.name}</option>`;
            }

            $$( '#producer-bank' ).html( markup ).trigger( 'change' );
        });
    });

    $$( '#producer-bank' ).off( 'change' ).on( 'change', function( event ) {
        let bankId = $$( this ).val();

        if ( bankId != '' ) {
            db.transaction( function( tx ) {
                tx.executeSql( 'SELECT * FROM banks WHERE remote_id = ?', [ bankId ], function( tx, result ) {
                    if ( result.rows.length != 0 ) {
                        tx.executeSql('SELECT * FROM bank_branches WHERE bank_id = ?', [result.rows.item(0).id], function (tx, result) {
                            if (result.rows.length > 0) {
                                let markup = ``;

                                for (i = 0; i < result.rows.length; i++) {
                                    let record = result.rows.item(i);

                                    markup += `<option value="${record.remote_id}">${record.name}</option>`;
                                }

                                $$('#bank-branch').html(markup).removeAttr('disabled').trigger('change');
                            }
                            else {
                                var emptyOption = `<option value="none"> No branches </option>`;
                                $$('#bank-branch').html(emptyOption).removeAttr('disabled').trigger('change');

                            }
                        });
                    }
                });
            });
        }
    });

    $$( '#bank_method' ).off( 'change' ).on( 'change', function( event ) {
        let bank_method = $$( this ).val();

        if (bank_method == 1  ) {
            $$('.bank').show();
            $$('.bank-branch').show();
            $$('.account-names').show();
            $$('.account-no').show();
            $$('.sacco').hide();
        }
        else {
            $$('.sacco').show();
            $$('.bank').hide();
            $$('.bank-branch').hide();
            $$('.account-names').hide();
            $$('.account-no').hide();
        }
    });

    db.transaction( function( tx ) {
        tx.executeSql( 'SELECT * FROM producer_groups', [], function( tx, result ) {
            let markup = `<option value="0">- Select -</option>
							<option value="0"> None </option>`;

            for ( let i = 0; i < result.rows.length; i++ ) {
                let record = result.rows.item( i );

                markup += `<option value="${record.remote_id}">${record.name}</option>`;
            }

            $$( '#producer-group' ).html( markup ).trigger( 'change' );
        });
    });

    db.transaction( function( tx ) {
        tx.executeSql( 'SELECT id, name FROM districts WHERE id IN ( ' + currentUser.districts.join( ', ' ) + ' )', [], function( tx, result ) {
            let markup = `<option value="none">- Select -</option>`;

            for ( let i = 0; i < result.rows.length; i++ ) {
                let record = result.rows.item( i );

                markup += `<option value="${record.id}">${record.name}</option>`;
            }

            $$( '#producer-district' ).html( markup ).trigger( 'change' );
        });
    });

    $$( '#group-name' ).on( 'blur', function( event ) {
        if ( $$( this ).val().trim().length === 0 )
            $$( this ).focus();
    }).focus();

    $$( '#producer-district' ).off( 'change' ).on( 'change', function( event ) {
        var districtId = $$( this ).val();

        db.transaction( function( tx ) {
            tx.executeSql( 'SELECT id, name FROM sub_counties WHERE district_id = ?', [ districtId ], function( tx, result ) {
                if ( result.rows.length > 0 ) {
                    let markup = ``;

                    for ( let i = 0; i < result.rows.length; i++ ) {
                        let record = result.rows.item( i );

                        markup += `<option value="${record.id}">${record.name}</option>`;
                    }

                    $$( '#producer-sub-county' ).html( markup ).removeAttr( 'disabled' ).trigger( 'change' );
                }
            });
        });
    });

    $$( '#producer-sub-county' ).off( 'change' ).on( 'change', function( event ) {
        var subCountyId = $$( this ).val();

        if ( subCountyId != '' ) {
            db.transaction( function( tx ) {
                tx.executeSql( 'SELECT id, name FROM villages WHERE sub_county_id = ?', [ subCountyId ], function( tx, result ) {
                    if ( result.rows.length > 0 ) {
                        let markup = ``;

                        for ( let i = 0; i < result.rows.length; i++ ) {
                            let record = result.rows.item( i );

                            markup += `<option value="${record.id}">${record.name}</option>`;
                        }

                        $$( '#producer-village' ).html( markup ).removeAttr( 'disabled' ).trigger( 'change' );
                    }
                    else {
                        var emptyOption = `<option value="none"> No villages </option>`;
                        $$( '#producer-village' ).html( emptyOption ).removeAttr( 'disabled' ).trigger( 'change' );
                    }
                });
            });
        }
    });

    $$( '#take-picture' ).off( 'click' ).on( 'click', function ( event ) {
        event.preventDefault();

        var camera_options = {
            quality: 100,
            targetWidth: 300,
            targetHeight: 300,
            allowEdit: true,
            correctOrientation: true,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.CAMERA,
            encodingType: Camera.EncodingType.JPEG,
            mediaType: Camera.MediaType.PICTURE,
        };

        navigator.camera.getPicture(
            function ( imageData ) {
                $$( '#myImage' ).attr( 'src', `data:image/jpeg;base64, ${imageData}` );
                $$( '#producer_photo' ).val( imageData );

            },
            null,
            camera_options
        );
    });

    $$( '#save-producer' ).off( 'click' ).on( 'click', function( event ) {

        var first_name = $$( '#first_name' ),
            other_names = $$( '#other_names' ),
            gender = $$( '#gender' ),
            d_o_b = $$( '#d_o_b' ),
            producer_phone_number = $$( '#producer_phone_number' ),
            producer_photo = $$( '#producer_photo' ),
            bank_method = $$( '#bank_method' ),
            producer_bank = $$( '#producer-bank' ),
            bank_branch = $$( '#bank-branch' ),
            bank_account_name = $$( '#producer_bank_name' ),
            bank_account_number = $$( '#producer_bank_account' ),
            producer_sacco = $$( '#producer_sacco' ),
            producer_group = $$( '#producer-group' ),
            position_held = $$( '#position_held' ),
            producer_address = $$( '#producer_address' ),
            district = $$( '#producer-district' ),
            subCounty = $$( '#producer-sub-county' ),
            village = $$( '#producer-village' ),
            groups = [];

        group_details = {
            group: producer_group.val(),
            position: position_held.val()
        };

        if ( bank_method.val() == 1 ) {

            bank_details = {
                method: 1,
                    banks:[
                        {
                            bank : producer_bank.val(),
                            branch: bank_branch.val(),
                            account: {
                                name: bank_account_name.val(),
                                number: bank_account_number.val()
                            }
                        }
                    ]
            };
        }
        else {

            bank_details = {
                method: 2,
                sacco: producer_sacco.val()
            };
        }

      groups.push( group_details );
     //   banks.push( bank_details );

        if ( first_name.val().trim().length === 0 ) {
            first_name.focus();
            first_name.focus().css({'border-bottom':'2px #ff2800 solid'});
        }
        else if ( other_names.val().trim().length === 0 ) {
            other_names.focus();
            other_names.focus().css({'border-bottom': '2px #ff2800 solid'});
        }
        else if ( gender.val().trim().length === 0 ) {
            gender.focus();
            gender.focus().css({'border-bottom':'2px #ff2800 solid'});
        }
        else if ( d_o_b.val().trim().length === 0 ) {
            d_o_b.focus();
            d_o_b.focus().css({'border-bottom': '2px #ff2800 solid'});
        }
        else if ( producer_phone_number.val().trim().length === 0 ) {
            producer_phone_number.focus();
            producer_phone_number.focus().css({'border-bottom': '2px #ff2800 solid'});
        }
		/*else if ( producer_photo.val().trim().length == 0 ){
		 producer_photo.focus();
		 producer_photo.focus().css({'border-bottom':'2px #ff2800 solid'});
		 }
		 */
        else if ( bank_method.val().trim().length === 0 ) {
            bank_method.focus();
            if ( bank_method.val() === 1  ){
                if ( producer_bank.val().trim().length() === 0 )
                    producer_bank.focus();

                if ( bank_branch.val().trim().length() === 0 )
                    bank_branch.focus();

                if ( bank_account_name.val().trim().length() === 0 ){
                    bank_account_name.focus();
                    bank_account_name.focus().css({'border-bottom':'2px #ff2800 solid'});
                }
                if ( bank_account_number.val().trim().length() === 0 ) {
                    bank_account_number.focus();
                    bank_account_number.css({'border-bottom':'2px #ff2800 solid'});
                }
            }
            else {

                if ( producer_sacco.val().trim().length() === 0 ) {
                    producer_sacco.focus();
                    producer_sacco.focus().css({'border-bottom':'2px #ff2800 solid'});
                }

            }
        }
		/*else if ( producer_group.val().trim().length == 1 )
		 {
		 if ( position_held.val().trim().length == 0 )
		 position_held.focus();
		 }
		 */
        else if ( producer_address.val().trim().length === 0 ) {
            producer_address.focus();
            producer_address.focus().css({'border-bottom':'2px #ff2800 solid'});

        }

        else if ( village.val() === '' ) {
            village.focus();
            village.focus().css({'border-bottom':'2px #ff2800 solid'});
        }
        else {

            db.transaction( function( tx ) {
                tx.executeSql( 'SELECT id FROM producers WHERE first_name = ? AND other_names = ?', [ first_name.val(), other_names.val() ], function( tx, result ) {
                    if ( result.rows.length == 0 ) {
                        tx.executeSql( 'INSERT INTO producers ( village_id, first_name , other_names , gender , date_of_birth , phone_number , photo , producer_groups , bank_details ,  created_at  , updated_at) VALUES (  ?, ? , ? ,? , ?, ? , ? ,? , ?, ? , ? )', [ parseInt( village.val() ), first_name.val(), other_names.val(), gender.val(), d_o_b.val() , producer_phone_number.val() , producer_photo.val() , JSON.stringify( groups ), JSON.stringify( bank_details ), get_current_timestamp() , get_current_timestamp() ],
                            function( tx, result ) {
                                $$( '.back' ).click();

                                loadProducers();
                              //  uploadData( 'producers' );
                            });
                    }
                    else {
                        app.alert( 'Producer is registered in the system!', 'Error' );

                    }
                });
            });
        }
    });
});

function loadProducer() {

    db.transaction( function( tx ) {
        tx.executeSql( 'SELECT * FROM producers WHERE id = ? LIMIT 1', [ producer ], function( tx, result ) {
            
            let producer = result.rows.item( 0 );
            $$( '.producer-names' ).text( result.rows.item( 0 ).first_name + ' ' + result.rows.item( 0 ).other_names );
            
            let markup = ``;
            
            if ( result.rows.length == 1 ) {

                let record = result.rows.item( 0 );
                let producer_groups = JSON.parse( record.producer_groups );
                let bank_details = JSON.parse ( record.bank_details );
                
                let producer_group = producer_groups.length > 0 ? producer_groups[0].group : 0;

                function getGender( id ) {
                    return ( id == 1 ) ? 'Female' : 'Male';
                }

                if( producer_groups.length > 0 )
					getGroup ( producer_groups[0].group );

                function getGroup( id  ) {

                    if ( id > 0 ) {

                        db.transaction( function( tx ) {
                            tx.executeSql( 'SELECT name FROM producer_groups WHERE remote_id = ? LIMIT 1', [ id ], function( tx, result ) {
                                if ( result.rows.length == 1 ) {

                                    $$( '#group-name' ).text( `Group: ${result.rows.item( 0 ).name}` );
                                    
                                    if( producer_groups[0].position == 1 )
                                        $$( '#position' ).text( `Position: Member`  ) ;
                                    
                                    else if( producer_groups[0].position == 2 )
                                        $$( '#position' ).text( `Position: Leader`  ) ;
                                    
                                    $$( '#producer_group' ).show();
                                    
                                }
                                else {
                                    $$( '#producer_group' ).hide();
                                }
                            });
                        });
                    }
                }

                getVillage ( record.village_id );

                function getVillage( id ) {

                    db.transaction( function( tx ) {
                        tx.executeSql( 'SELECT name FROM villages WHERE id = ? LIMIT 1', [ id ], function( tx, result ) {
                            if ( result.rows.length == 1 ) {
                                $$( '.village' ).text( result.rows.item( 0 ).name );
                            }
                        });
                    });

                }
                
                getBankingMethod( bank_details.method );

				function getBankingMethod( method ) {

					if( method == 1 ) {
						
                        
						var bank_id = bank_details.banks[0].bank ,
						branch_id =   bank_details.banks[0].branch;

						db.transaction( function( tx ) {
							tx.executeSql( 'SELECT name FROM banks WHERE remote_id = ? LIMIT 1', [ bank_id ], function( tx, result ) {
								if ( result.rows.length == 1 ) {
                                    $$( '.sacco-name' ).hide();
                                    $$( '.bank-heading' ).text( `Banking Details` );
									$$( '.bank-name' ).text( `Bank : ${result.rows.item( 0 ).name}` );
									$$( '.account-name' ).text( `Account Name : ${bank_details.banks[0].account.name}` );
									$$( '.account-number' ).text( `Account Number : ${bank_details.banks[0].account.number}` );

								}
							});
						});

						db.transaction( function( tx ) {
							tx.executeSql( 'SELECT name FROM bank_branches WHERE remote_id = ? LIMIT 1', [ branch_id ], function( tx, result ) {
								if ( result.rows.length == 1 )
                                    $$( '.branch-name' ).text( 'Branch : ' + result.rows.item( 0 ).name );

							});
						});

					} 
                    else
                        $$( '.branch-name, .account-name, .account-number, .bank-name' ).hide();
				 
                }

                markup = `<div class="content-block">
								<img src="data:image/jpeg;base64,${record.photo}" class="img-fluid" />
							</div>
							<div class="content-block-title"> Name </div>
							<div class="content-block">
								<p>${record.first_name} ${record.other_names}</p>
							</div>
							<div class="content-block-title"> Gender </div>
							<div class="content-block">
								<p>${getGender( record.gender )}</p>
							</div>
							<div class="content-block-title">Date of birth</div>
							<div class="content-block">
								<p>${record.date_of_birth}</p>
							</div>
							<div class="content-block-title">
								Phone number
							</div>
							<div class="content-block">
								<p>${record.phone_number}</p>
							</div>
							<div class="content-block-title">
								Village
							</div>
							<div class="content-block">
								<p class="village"></p>
							</div>
							<div class="bank-heading content-block-title">
								Sacco Name 
							</div>
							<div class="content-block">
								<p class="bank-name"></p>
								<p class="branch-name"></p>
								<p class="account-name"></p>
								<p class="account-number"></p>
								<p class="sacco-name">${bank_details.sacco}</p>
							</div>
						    <div id="producer_group">
                                <div class="content-block-title">
                                    Group information
                                </div>
                                <div class="content-block">
                                    <p id="group-name"></p>
                                    <p id="position"></p>
                                </div>
                            </div>`;
                
                $$( '.page[data-page="producer"]  .tabs-swipeable-wrap .tabs #producer-info-1' ).html( markup );
            }
        });
    });

    db.transaction( function( tx ) {
        tx.executeSql( "SELECT  areas.tgb_area_size , areas.land_system , land_systems.name as land_system , areas.gps , areas.land_size , areas.village_id , villages.name as village_name ,areas.producer_id   FROM areas  JOIN villages ON villages.id  = areas.village_id JOIN land_systems ON land_systems.id  = areas.land_system  WHERE areas.producer_id = ? ", [ producer ], function( tx, result ) {
            let markup = ``;

            if ( result.rows.length == 1 ) {

                for ( i = 0; i < result.rows.length; i++ ) {
                    let record = result.rows.item( 0 );

                    markup = `<div class="content-block-title"> Village </div>
                        <div class="content-block">
                            <p> ${record.village_name}</p>
                        </div>
                        <div class="content-block-title"> Land use system </div>
                        <div class="content-block">
                            <p>${record.land_system}</p>
                        </div>
                        <div class="content-block-title"> Total Land size (ha)</div>
                        <div class="content-block">
							<p>${record.land_Size}</p>
                        </div>
                        <div class="content-block-title"> Area under TGB (ha)</div>
                        <div class="content-block">
                            <p>${record.tgb_area_size}</p>
                        </div>
                        <div class="content-block-title"> GPS Coordinates </div>
                        <div class="content-block">
							<p>${record.gps}</p>
                        </div>`;

                    $$( '.page[data-page="producer"] .tabs-swipeable-wrap .tabs #producer-area-info-2' ).html( markup );
                }
            }
        });
    });

    function getBoolean( id ) {
        return ( id == 1 ) ? `Yes` : `No`;
    }

    db.transaction( function( tx ) {

        tx.executeSql( 'SELECT  * FROM plantings WHERE producer_id = ? ', [ producer ], function( tx, result ) {

            let markup = ``;

            for ( i = 0; i < result.rows.length; i++ ) {

                let record = result.rows.item( i ), planted_tree_species = JSON.parse( record.planted_species );

                //app.alert( planted_tree_species[0].tree_species );

                getTreeSpecies ();

                function getTreeSpecies( id ) {

                    db.transaction( function( tx ) {
                        tx.executeSql( 'SELECT name FROM villages WHERE id = ? LIMIT 1', [ id ], function( tx, result ) {
                            if ( result.rows.length == 1 )
                                $$( '.village' ).text( result.rows.item( 0 ).name );

                        });
                    });

                }

                markup = `<div class="content-block-title">  Planting year </div>
						<div class="content-block">
							<p>${record.id}</p>
						</div>
						<div class="content-block-title"> Planting started</div>
						<div class="content-block">
							<p>${getBoolean( record.planting_started )}</p>
						</div>
						<div class="content-block-title"> Tree Target</div>
						<div class="content-block">
							<p>${record.tree_target}</p>
						</div>
						<div class="content-block-title"> Spacing </div>
						<div class="content-block">
							<p>${record.spacing}</p>
						</div>
						<div class="content-block-title"> Planted Species </div>
						<div class="content-block">
							<p>${planted_tree_species.length}</p>
						</div>`;

            }

            $$( '.page[data-page="producer"] .tabs-swipeable-wrap .tabs #producer-area-info-2' ).html( markup );

        });
    });

    db.transaction( function( tx ) {
        tx.executeSql( "SELECT monitorings.notes , monitorings.qualify , monitorings.tree_samples ,   monitorings.number_of_seeds ,  monitorings.date_taken , monitorings.seedling_source , monitorings.year , monitorings.tree_target , monitorings.correct_area , monitorings.correct_tree_species , monitorings.causes_of_trees_dying , monitorings.main_corrective_action , corrective_actions.name as action_name , causes.name as causes_name   FROM monitorings  JOIN corrective_actions ON corrective_actions.id  = monitorings.main_corrective_action JOIN causes ON causes.id  = monitorings.causes_of_trees_dying WHERE  monitorings.producer_id = ? ", [ producer ], function( tx, result ) {

            let markup = ``;

            if ( result.rows.length == 1 ) {
                for ( let i = 0; i < result.rows.length; i++ ) {
                    let record = result.rows.item( 0 );
                    markup = `<div class="content-block-title">  Monitoring year </div>
							<div class="content-block">
								<p>${record.year}</p>
							</div>
							<div class="content-block-title"> Tree target</div>
							<div class="content-block">
								<p>${getBoolean( record.tree_target )}</p>
							</div>
							<div class="content-block-title"> Correct area</div>
							<div class="content-block">
								<p> ${getBoolean( record.correct_area )}</p>
							</div>
							<div class="content-block-title"> Correct tree species </div>
							<div class="content-block">
								<p>${getBoolean( record.correct_tree_species )}</p>
							</div>
							<div class="content-block-title"> Cause of tree dying </div>
							<div class="content-block">
								<p>${record.causes_name}</p>
							</div>
							<div class="content-block-title"> Main Corrective Action </div>
							<div class="content-block">
							<p>${record.action_name}</p>
							</div>
							<div class="content-block-title"> Date taken </div>
							<div class="content-block">
								<p>${record.date_taken}</p>
							</div>
							<div class="content-block-title"> Seedling source </div>
							<div class="content-block">
								<p>${record.seedling_source}</p>
							</div>
							<div class="content-block-title"> Number of seeds </div>
							<div class="content-block">
								<p>${record.number_of_seeds}</p>
							</div>
							<div class="content-block-title"> Comments </div>
							<div class="content-block">
								<p>${record.notes}</p>
							</div>
							<div class="content-block-title"> Does producer qualify </div>
							<div class="content-block">
								<p>${getBoolean( record.qualify  )}</p>
							</div>
							<div class="content-block-title">Tree sample plots taken </div>
							<div class="content-block">
								<p>${record.tree_samples}</p>
							</div>`;

                    $$( '.page[data-page="producer"] .page-content .tabs #producer-monitoring-info-4' ).html( markup );
                }
            }
        });
    });

}

$$( document ).on( 'page:init', '.page[data-page="producer"] ', function( event ) {
    loadProducer();
});

function loadAreas() {

    db.transaction( function( tx ) {

        tx.executeSql( "SELECT  producers.photo, areas.tgb_area_size , areas.gps , areas.village_id , villages.name as village_name ,areas.producer_id  , producers.first_name , producers.other_names   FROM areas JOIN producers  ON areas.producer_id = producers.id JOIN villages ON villages.id  = areas.village_id  ", [  ], function( tx, result ) {
            let markup = ``;

            for ( let i = 0; i < result.rows.length; i++ ) {
                let record = result.rows.item( i );
                markup += `<li>
                        <a class="item-link item-content">
						<div class="item-media">
						    ${record.first_name.charAt(0)} ${record.other_names.charAt(0)}
						</div>
						<div class="item-inner">
							<div class="item-title-row">
								<div class="item-title">
									 ${record.first_name}
								</div>
							</div>
							<div class="item-subtitle">
								${record.village_name}
							</div>
						</div>
				</a></li>`;

            }

            $$( '.areas-list-block ul' ).html( markup );

        });
    });
}

$$( document ).on( 'page:init', '.page[data-page="areas"]', function( event ) {
    loadAreas();
});

$$( document ).on( 'page:init', '.page[data-page="add-area"]', function( event ) {

    db.transaction( function( tx ) {
        tx.executeSql( 'SELECT id, first_name, other_names FROM producers ORDER BY first_name ASC ', [], function( tx, result) {

            let markup = `<option value="none"> - Select producer - </option>`;

            for ( let i = 0; i < result.rows.length; i++ ) {
                let record = result.rows.item( i );
                markup += `<option value="${record.id}">${record.first_name} ${record.other_names}</option>`;
            }

            $$( '#producer-name' ).html( markup ).trigger( 'change' );
        });
    });

    db.transaction( function( tx ) {
        tx.executeSql( 'SELECT id, name FROM land_systems ORDER BY name ASC ', [ ], function( tx, result ) {
            let markup = `<option value="none"> - Select - </option>`;

            for ( i = 0; i < result.rows.length; i++ ) {
                let record = result.rows.item( i );
                markup += `<option value="${record.id}">${record.name}</option>`;
            }

            $$( '#land-systems' ).html( markup ).trigger( 'change' );
        });
    });

    db.transaction( function( tx ) {
        tx.executeSql( 'SELECT id, name FROM districts WHERE id IN ( ' + currentUser.districts.join( ', ' ) + ' )', [], function( tx, result ) {
            let markup = `<option value="none"> - Select - </option>`;

            for ( i = 0; i < result.rows.length; i++ ) {
                let record = result.rows.item( i );

                markup += `<option value="${record.id}">${record.name}</option>`;
            }

            $$( '#area-district' ).html( markup ).trigger( 'change' );
        });
    });

    $$( '#area-district' ).off( 'change' ).on( 'change', function( event ) {
        var districtId = $$( this ).val();

        db.transaction( function( tx ) {
            tx.executeSql( 'SELECT id, name FROM sub_counties WHERE district_id = ?', [ districtId ], function( tx, result ) {
                if ( result.rows.length > 0 ) {
                    let markup =``;

                    for ( i = 0; i < result.rows.length; i++ ) {
                        let record = result.rows.item( i );

                        markup += `<option value="{$record.id}">${record.name}</option>`;
                    }

                    $$( '#area-sub-county' ).html( markup ).removeAttr( 'disabled' ).trigger( 'change' );
                }
            });
        });
    });

    $$( '#area-sub-county' ).off( 'change' ).on( 'change', function( event ) {
        var subCountyId = $$( this ).val();

        if ( subCountyId != '' ) {
            db.transaction( function( tx ) {
                tx.executeSql( 'SELECT id, name FROM villages WHERE sub_county_id = ?', [ subCountyId ], function( tx, result ) {
                    if ( result.rows.length > 0 ) {
                        let markup = ``;

                        for ( i = 0; i < result.rows.length; i++ ) {
                            let record = result.rows.item( i );

                            markup += `<option value="{$record.id}">${record.nam}</option>`;
                        }

                        $$( '#area-village' ).html( markup ).removeAttr( 'disabled' ).trigger( 'change' );
                    }
                    else{
                        let emptyOption = '<option value="none"> Villages not found </option> ';
                        $$( '#area-village' ).html( emptyOption ).removeAttr( 'disabled' ).trigger( 'change' );
                    }
                });
            });
        }
    });
    $$('#get-gps').off( 'click' ).on( 'click' , function ( event ){

        navigator.geolocation.getCurrentPosition(onSuccess, onError);

    });

    $$( '#save-area' ).off( 'click' ).on( 'click', function( event ) {

        var producer_name = $$( '#producer-name' ),
            tgb_area_size = $$( '#tgb-area-size' ),
            area_gps = $$( '#get_area_gps' ),
            total_land_size = $$( '#total-land-size' ),
            district = $$( '#area-district' ),
            land_systems = $$( '#land-systems' ),
            subCounty = $$( '#area-sub-county' ),
            village = $$( '#area-village' );

        if ( producer_name.val().trim().length === 0 ) {
            producer_name.focus();
            producer_name.focus().css({'border-bottom':'2px #ff2800 solid'});

        }
        else if (  tgb_area_size.val() === '' ) {
            tgb_area_size.focus();
            tgb_area_size.focus().css({'border-bottom':'2px #ff2800 solid'});

        }
        else if ( area_gps.val() === '' ) {
            area_gps.focus();
            area_gps.focus().css({'border-bottom':'2px #ff2800 solid'});
        }
        else if ( total_land_size.val() === '' ) {
            total_land_size.focus();
            total_land_size.focus().css({'border-bottom':'2px #ff2800 solid'});
        }
        else if (  village.val() === '' ) {
            village.focus();
            village.focus().css({'border-bottom':'2px #ff2800 solid'});
        }
        else {
            db.transaction( function( tx ) {
                tx.executeSql( 'SELECT * FROM areas WHERE producer_id = ? AND village_id = ?', [producer_name.val(), village.val() ], function( tx, result ) {
                    if ( result.rows.length == 0 ) {
                        tx.executeSql( 'INSERT INTO areas ( producer_id , land_system , tgb_area_size, gps , land_Size , village_id ,  created_at  , updated_at ) VALUES (  ? , ?, ? , ? , ? ,? , ? ,? )', [ producer_name.val()  ,land_systems.val() ,  tgb_area_size.val() , area_gps.val() , total_land_size.val() ,  village.val() , get_current_timestamp() , get_current_timestamp() ], function( tx, result ) {
                            $$( '.back' ).click();

                            loadAreas();
                        });
                    } else
                        app.alert( 'Area for that producer is already registered in the system!', 'Error' );


                });
            });
        }
    });
});

function onSuccess( position ) {
    $$('#get_area_gps').val( `${position.coords.latitude} ${position.coords.longitude}` );
}

function onError( error ) {
    app.alert( `Code: ${error.code} <br/> Message: ${error.message}` );

}

function loadPlantings() {

    db.transaction( function( tx ) {
        tx.executeSql( "SELECT DISTINCT id, remote_id , producer_id , area_id  FROM plantings ORDER BY id DESC", [], function( tx, result ) {
            let markup = ``;

            for ( i = 0; i < result.rows.length; i++ ) {
                let record = result.rows.item( i );

                getProducer( record.producer_id );

                function getProducer( id ) {

                    db.transaction( function( tx ) {
                        tx.executeSql( 'SELECT * FROM producers WHERE id = ? ', [ id ], function( tx, result ) {
                            for (let i = 0; i < result.rows.length; i++ ) {
                                let producer = result.rows.item( 0 );

                                $$( '.producer-name' ).text( `${producer.first_name}  ${producer.other_names}` );

                            }
                        });
                    });
                }

                markup	+=	`<li class="item-content">
								<div class="item-inner">
									<div class="item-title-row">
										<div class="item-title">
											<span class="producer-name"></span>
										</div>
									</div>
									<div class="item-subtitle">
										${record.area_id}
									</div>
								</div>
							</li>`;
            }

            $$( '.plantings-list-block ul' ).html( markup );

        });
    });
}

$$( document ).on( 'page:init', '.page[data-page="plantings"]', function( event ) {
    loadPlantings();
});

$$( document ).on( 'page:init', '.page[data-page="choose-planting"]', function( event ) {
    $$( '#planting-button' ).hide();
    $$( '#planting-completed' ).hide();

    $$( '#area-id' ).on( 'change', function() {

        setYear( 'plantings', $$( this ).val() ).then( function( data ) {

            if( typeof( data ) !== 'undefined' ) {

                $$( '#planting-button' ).attr('href','pages/add-planting.html' ).text( `Planting Year ${ parseInt( data ) < 1 ? 0 : data  }` ).show();
                $$( '#next_year').val( `${ parseInt( data ) < 1 ? 0 : data  }` );
                $$( '#planting-completed' ).hide();

            }
            else
                $$( '#planting-completed' ).show();

        }).catch(function(err) {

            $$( '#planting-button' ).hide();
            $$( '#planting-completed' ).hide();

        });
    });

    db.transaction( function( tx ) {
        tx.executeSql( 'SELECT id, first_name , other_names FROM producers ORDER BY first_name ASC ', [], function( tx, result ) {
            let markup = `<option value="none"> - Select producer - </option>`;

            for ( let i = 0; i < result.rows.length; i++ ) {
                let record = result.rows.item( i );

                markup += `<option value="${record.id}"> ${record.first_name} ${record.other_names} </option>`;
            }

            $$( '#producer-id' ).html( markup ).trigger( 'change' );

        });
    });

    $$( '#producer-id' ).off( 'change' ).on( 'change', function( event ) {
        event.preventDefault();

        let producerId = $$( this ).val();

        db.transaction( function( tx ) {
            tx.executeSql( "SELECT areas.id, producers.photo, areas.tgb_area_size , areas.gps , areas.village_id , villages.name as village_name ,areas.producer_id  , producers.first_name , producers.other_names   FROM areas JOIN producers  ON areas.producer_id = producers.id JOIN villages ON villages.id  = areas.village_id WHERE producer_id = ? ", [ producerId ], function( tx, result ) {
                if ( result.rows.length > 0 ) {
                    let markup = `<option value="none">Select village</option>`;

                    for ( i = 0; i < result.rows.length; i++ ) {
                        let record = result.rows.item( i );

                        markup += `<option value="${record.id}">${record.village_name}</option>`;
                    }

                    $$( '#area-id' ).html( markup ).removeAttr( 'disabled' ).trigger( 'change' );
                }
                else {
                    let emptyOption = `<option value="none"> Villages not found </option>`;
                    $$( '#area-id' ).html( emptyOption ).removeAttr( 'disabled' ).trigger( 'change' );
                    $$( '#planting-button').hide();

                }
            });
        });
    });

    $$( '#planting-button' ).off( 'click' ).on( 'click', function( event ) {

        localStorage.setItem('planting_producer_id' , $$('#producer-id').val() );
        localStorage.setItem('planting_area_id' , $$( '#area-id' ).val() );
        localStorage.setItem( 'next_year', $$( '#next_year' ).val() );

    });

});

$$( document ).on( 'page:init', '.page[data-page="add-planting"]', function( event ) {

    $$( '.page[data-page="add-planting"]' ).on( 'keyup', '.tree_specie_no', function() {
        calculateSum();

    });

    function calculateSum() {
        let sum = 0;
        $$( '.tree_specie_no' ).each( function() {
            if( !isNaN( this.value ) && this.value.length != 0 )
                sum += parseFloat( this.value );

        });

        $$( '#total-trees' ).html( sum.toFixed(0) );

    }

    db.transaction( function( tx ) {
        tx.executeSql( 'SELECT id, name  FROM tree_species ORDER BY name ASC ', [ ], function( tx, result ) {
            let markup = ``;
            for ( let i = 0; i < result.rows.length; i++ ) {
                let record = result.rows.item( i );

                markup += `<option value="${record.id}">${record.name}</option>`;
            }

            $$( '#add-tree' ).off( 'click' ).on( 'click', function( event ) {

                let next_id = $$( '.tree-specie-row' ).length + 1;

                $$( '#tree-items' ).append(
                    ( function( id ) {
                            id = id || 0;
                            return `<div class="tree-specie-row" >
									<div class="item-title label" >
										<div class="item-input">
											<select class="tree_specie_name" id="tree_specie_name[]">
												${markup}
											</select>
										</div>
										<div class="item-input">
											<input type="number" class="tree_specie_no"  id="tree_specie_id_'+id+'" placeholder="Enter number" />
										</div>
									</div>
								</div>`;
                        }( next_id )
                    )
                );
            });
        });
    });

    $$( '#save-planting' ).off( 'click' ).on( 'click', function( event ) {
        let planting_started = $$( '#planting-started' ), tree_target = $$( '#tree-target' ), area = $$('#area-id' ),
            spacing = $$( '#spacing' ), species_name =[], species_qty = [], planted = [], tree_species = [], tree = {};

        $$('.tree_specie_name ').each(function(i, e) {
            species_name.push( $$( this ).val() );

        });
        //Try to select closest child
        $$( '.tree_specie_no' ).each(function(i, e) {
            species_qty.push( $$(this).val() );

        });

        for( i = 0; i < species_name.length; i++ ) {
            species = {
                name : species_name[i],
                qty : species_qty[i]
            }

            planted.push( species );
        }

        db.transaction( function( tx ) {

            tx.executeSql( 'INSERT INTO plantings ( producer_id, area_id,  year, planting_started, tree_target, spacing, planted_species ,  created_at  , updated_at ) VALUES ( ?, ? , ?, ? , ? , ? ,? , ? ,? )',
                [ localStorage.getItem('planting_producer_id'), localStorage.getItem( 'planting_area_id' ), localStorage.getItem( 'next_year' ),  planting_started.val(), tree_target.val(), spacing.val(), JSON.stringify( planted ), get_current_timestamp(), get_current_timestamp() ], function( tx, result ) {
                    $$( '.back' ).click();

                    loadPlantings();
                });
        });
    });
});

function loadMonitorings() {
    db.transaction( function( tx ) {
        tx.executeSql( "SELECT DISTINCT  producers.first_name  as first_name, producers.other_names as other_names , monitorings.id , monitorings.producer_id , monitorings.remote_id FROM monitorings JOIN producers on monitorings.producer_id  = producers.id ", [], function( tx, result ) {
            let markup = ``;

            for ( let i = 0; i < result.rows.length; i++ ) {
                let record = result.rows.item( i );

                markup += `<li class="item-content">
						<div class="item-inner">
							<div class="item-title-row">
								<div class="item-title">
									${record.first_name} ${record.other_names}
								</div>
							</div>
							<div class="item-subtitle">
								${record.first_name}
							</div>
						</div>
				</li>`;
            }

            $$( '.monitoring-list-block ul' ).html( markup );

        });
    });
}

$$( document ).on( 'page:init', '.page[data-page="monitoring"]', function( event ) {
    loadMonitorings();
});

$$( document ).on( 'page:init', '.page[data-page="choose-monitoring"]', function( event ) {
    $$( '#monitor-button' ).hide();

    $$( '#area-id' ).on( 'change', function() {
        setYear( $$( this ).val() );

        if( /*parseInt( window.localStorage.getItem('next_year') )*/ next_year == 0 )
            $$( '#monitor-button' ).attr('href','pages/add-monitoring-0-3.html' ).text( 'Monitor Year 0' ).show();

        else if ( /*parseInt( window.localStorage.getItem('next_year') )*/ next_year == 1 )
            $$( '#monitor-button' ).attr('href','pages/add-monitoring-0-3.html' ).text( 'Monitor Year 1' ).show();

        else if ( /*parseInt( window.localStorage.getItem('next_year') )*/ next_year == 3 )
            $$( '#monitor-button' ).attr('href','pages/add-monitoring-0-3.html' ).text( 'Monitor Year 3' ).show();

        else if( /*parseInt( window.localStorage.getItem('next_year') )*/ next_year == 5 )
            $$( '#monitor-button' ).attr('href','pages/add-monitoring-5-10.html' ).text( 'Monitor Year 5' ).show();

        else if ( /*parseInt( window.localStorage.getItem('next_year') )*/ next_year == 7 )
            $$( '#monitor-button' ).attr('href','pages/add-monitoring-5-10.html' ).text( 'Monitor Year 7' ).show();

        else if ( /*parseInt( window.localStorage.getItem('next_year') )*/ next_year == 10 )
            $$( '#monitor-button' ).attr('href','pages/add-monitoring-5-10.html' ).text( 'Monitor Year 10' ).show();

        else
            $$( '#monitor-button' ).hide();

    });

    db.transaction( function( tx ) {

        tx.executeSql( 'SELECT id, first_name , other_names FROM producers ORDER BY first_name ASC ', [], function( tx, result ) {
            let markup = `<option value="none">- Select -</option>`;

            for ( let i = 0; i < result.rows.length; i++ ) {
                let record = result.rows.item( i );

                markup += `<option value="${record.id}">${record.first_name} ${record.other_names}</option>`;
            }

            $$( '#producer-id' ).html( markup ).trigger( 'change' );
        });
    });

    $$( '#producer-id' ).off( 'change' ).on( 'change', function( event ) {
        event.preventDefault();

        let producerId = $$( this ).val();

        db.transaction( function( tx ) {
            tx.executeSql( "SELECT areas.id, producers.photo, areas.tgb_area_size , areas.gps , areas.village_id , villages.name as village_name ,areas.producer_id  , producers.first_name , producers.other_names   FROM areas JOIN producers  ON areas.producer_id = producers.id JOIN villages ON villages.id  = areas.village_id WHERE producer_id = ? ", [ producerId ], function( tx, result ) {
                if ( result.rows.length > 0 ) {
                    let markup = ``;

                    for ( let i = 0; i < result.rows.length; i++ ) {
                        let record = result.rows.item( i );

                        markup += `<option value="${record.id}">${record.village_name}</option>`;
                    }

                    $$( '#area-id' ).html( markup ).removeAttr( 'disabled' ).trigger( 'change' );
                }
                else {
                    let emptyOption = `<option value=""> Villages not found </option>`;
                    $$( '#area-id' ).html( emptyOption ).removeAttr( 'disabled' ).trigger( 'change' );
                    $$( '#monitor-button').hide();

                }
            });
        });
    });

    $$( '#monitor-button' ).off( 'click' ).on( 'click', function( event ) {
        window.localStorage.setItem('monitoring_producer_id' , $$('#producer-id').val() );
        window.localStorage.setItem('monitoring_area_id' , $$('#area-id').val() );

    });
});

$$( document ).on( 'page:init', '.page[data-page="add-monitoring"]', function( event ) {

    $$( 'li.hide_message').show();
    $$( 'li#hide_correct-areas-planted , div#save_button, li#hide_correct-tree-species-planted,li#hide_does_producer_qualify,li#hide_comments  , li#hide_main-cause-tree-dying , li#hide_main-corrective-action , li#hide_date-taken , li#hide_seedling-sources  , li#hide_number-of-seeds').hide();

    $$( '#tree-target-met' ).change( function( event ) {

        var checked = $$( this ).prop( 'checked' ) == true ? 1 : 2;

        if( checked == 1) {
            $$( 'li#hide_correct-areas-planted , div#save_button , li#hide_does_producer_qualify,li#hide_comments,  li#hide_correct-tree-species-planted , li#hide_main-cause-tree-dying , li#hide_main-corrective-action , li#hide_date-taken , li#hide_seedling-sources  , li#hide_number-of-seeds').show();
            $$( 'li.hide_message').hide();

        }
        else {
            $$( 'li#hide_correct-areas-planted , div#save_button ,li#hide_does_producer_qualify,li#hide_comments, li#hide_correct-tree-species-planted, li#hide_main-corrective-action , li#hide_main-cause-tree-dying , li#hide_date-taken , li#hide_seedling-sources  , li#hide_number-of-seeds').hide();
            $$( 'li.hide_message').show();

        }
    });

    $$('li#hide_number-of-seeds , li#hide_seedling-sources').hide();

    var  samples = $$( '#samples') ,
        tree_items_snippet = `<div id="tree-samples" class="tree-samples row">
	                    <div class="col-30">
	                        <div class="item-title label">
	                            Height(m)
	                        </div>
	                        <div class="item-input">
	                            <input type="number" id="height" class="height"  />
	                       </div>
	                    </div>
	                    <div class="col-30">
	                        <div class="item-title label">
	                            Crown (m)
	                        </div>
	                        <div class="item-input">
	                            <input type="number" id="crown"  class="crown" />
	                        </div>
	                    </div>
	                    <div class="col-40">
	                        <div class="item-title label">
	                       		Tree Growth (DBH)
	                    	</div>
	                       	<div class="item-input">
	                           	<select id="tree-growth" class ="dbh">
	                            	<option value="">- Select -</option>
	                            	<option value="5-"> <i> &#8804;</i>5 </option>
	                            	<option value="6-9">6 - 9</option>
	                            	<option value="10-19">10- 19</option>
	                            	<option value="20-29"> 20 - 29 </option>
	                            	<option value="30+">  <i> &#8805;</i> 30 </option>
	                        	</select>
	                    	</div>
	                	</div>
	                </div><br>`;

    $$("#add-sample").off( 'click' ).on( 'click', function( event )  {
        samples.append( tree_items_snippet );
    });

    db.transaction( function( tx ) {
        tx.executeSql( 'SELECT id, name  FROM corrective_actions ORDER BY name ASC ', [], function( tx, result ) {
            let markup = `<option value="none">- Select -</option>`;

            for ( let i = 0; i < result.rows.length; i++ ) {
                let record = result.rows.item( i );

                markup += `<option value="${record.id}">${record.name}</option>`;
            }

            $$( '#main-corrective-action' ).html( markup ).removeAttr( 'disabled' ).trigger( 'change' );
        });
    });

    db.transaction( function( tx ) {
        tx.executeSql( 'SELECT id, name  FROM causes ORDER BY name ASC ', [ ], function( tx, result ) {
            let markup = `<option value="none">- Select -</option>`;

            for ( let i = 0; i < result.rows.length; i++ ) {
                let markup = result.rows.item( i );

                markup += `<option value="${record.id}">${record.name}</option>`;
            }
            $$( '#main-cause-tree-dying' ).html( markup ).removeAttr( 'disabled' ).trigger( 'change' );

        });
    });

    $$( '#main-corrective-action' ).off( 'change' ).on( 'change', function( event ) {
        let correctiveAction = $$( this ).val();

        if( correctiveAction === 3 ) {
            $$('li#hide_number-of-seeds , li#hide_seedling-sources').show();
            $$('li#hide_other-actions').hide();
        }
        else
            $$('li#hide_number-of-seeds , li#hide_seedling-sources').hide();

        if( correctiveAction === 2 )
            $$('li#hide_other-actions').show();

        else
            $$('li#hide_other-actions').hide();
    });

    $$( '#main-cause-tree-dying' ).off( 'change' ).on( 'change', function( event ) {
        let cause = $$( this ).val();

        if( cause === 4 )
            $$('li#hide_other-cause').show();
        else
            $$('li#hide_other-cause').hide();

    });

    $$( '#save-monitoring' ).off( 'click' ).on( 'click', function( event ) {
        let correct_areas_planted = $$( '#correct-areas-planted' ).prop( 'checked' ) === true ? 1 : 2 ,
            tree_target_met = $$( '#tree-target-met' ).prop( 'checked' ) === true ? 1 : 2 ,
            correct_tree_species_planted = $$( '#correct-tree-species-planted' ).prop( 'checked' ) === true ? 1 : 2 ,
            main_corrective_action = $$( '#main-corrective-action' ),
            other_corrective_action = $$( '#other-actions' ),
            date_taken = $$( '#date-taken' ),
            seedling_sources = $$( '#seedling-sources' ),
            number_of_seeds = $$( '#number-of-seeds' ),
            comments = $$( '#comments'),
            trees_dying = $$( '#main-cause-tree-dying'),
            other_causes = $$( '#other-causes'),
            does_producer_qualify = $$( '#does-producer-qualify');

        let height = [], crown = [] , dbh = [];
        $$('.crown ').each(function(i, e) {
            crown.push( $$( this).val() );

        });
        $$('.height').each(function(i, e) {
            height.push( $$(this).val() );

        });
        $$('.dbh').each(function(i, e) {
            dbh.push( $$(this).val() );

        });

        let tree_samples = [];

        for( let i = 0; i < height.length; i++ )
            tree_samples.push( '{"height":"'+height[i]+'","crown":"'+crown[i]+'","tree_growth":"'+dbh[i]+'"}' );

        var sample_object = "["+tree_samples.join()+"]";

        db.transaction( function( tx ) {
            tx.executeSql( ' INSERT INTO monitorings ( producer_id ,  area_id  , year  , tree_target  , correct_area  ,correct_tree_species  , main_corrective_action  , causes_of_trees_dying ,date_taken , seedling_source  , number_of_seeds  ,   notes  , qualify  , tree_samples  , created_at  , updated_at  )  VALUES ( ?, ? , ?  ,  ? , ? , ?, ?, ? , ? , ? , ? , ? , ? , ? ,? ,?  )',
                [localStorage.getItem('monitoring_producer_id'), localStorage.getItem('monitoring_area_id'), next_year, tree_target_met, correct_areas_planted, correct_tree_species_planted, main_corrective_action.val(), trees_dying.val(), date_taken.val(), seedling_sources.val(), number_of_seeds.val(), comments.val(), does_producer_qualify.val(), sample_object, get_current_timestamp(), get_current_timestamp() ], function( tx, result ) {
                    $$( '.back' ).click();

                    loadMonitorings();
                });

        });
    });
});

function loadSocioEconomic() {

}

$$( document ).on( 'page:init', '.page[data-page="choose-socio-economic"]', function( event ) {
    $$( '#socio-economic-button' ).hide();

    $$( '#area-id' ).on( 'change', function() {
        setYear( $$( this ).val() );

        if ( /*parseInt( window.localStorage.getItem('next_year') )*/ next_year == 0 )
            $$( '#socio-economic-button' ).attr('href','pages/socio-economic.html' ).text( ' Year 1' ).show();

        else if ( /*parseInt( window.localStorage.getItem('next_year') )*/ next_year == 3 )
            $$( '#socio-economic-button' ).attr('href','pages/socio-economic.html' ).text( ' Year 3' ).show();

        else
            $$( '#socio-economic-button' ).hide();

    });

    db.transaction( function( tx ) {
        tx.executeSql( 'SELECT id, first_name, other_names FROM producers ORDER BY first_name ASC ', [ ], function( tx, result ) {
            let markup = `<option value="none">- Select -</option>`;

            for ( let i = 0; i < result.rows.length; i++ ) {
                let record = result.rows.item( i );

                markup += `<option value="${record.id}">${record.first_name} ${record.other_names}</option>`;
            }

            $$( '#producer-id' ).html( markup ).trigger( 'change' );
        });
    });

    $$( '#producer-id' ).off( 'change' ).on( 'change', function( event ) {
        event.preventDefault();

        let producerId = $$( this ).val();

        db.transaction( function( tx ) {
            tx.executeSql( 'SELECT areas.id, producers.photo, areas.tgb_area_size, areas.gps, areas.village_id, villages.name as village_name, areas.producer_id, producers.first_name, producers.other_names FROM areas JOIN producers ON areas.producer_id = producers.id JOIN villages ON villages.id  = areas.village_id WHERE producer_id = ? ', [ producerId ], function( tx, result ) {
                if ( result.rows.length > 0 ) {
                    let markup = ``;

                    for ( i = 0; i < result.rows.length; i++ ) {
                        let record = result.rows.item( i );

                        markup += `<option value="${record.id}">${record.village_name}</option>`;
                    }

                    $$( '#area-id' ).html( markup ).removeAttr( 'disabled' ).trigger( 'change' );
                }
                else {
                    let emptyOption = `<option value=""> Village not found </option>`;
                    $$( '#area-id' ).html( emptyOption ).removeAttr( 'disabled' ).trigger( 'change' );
                    $$( '#socio-economic-button').hide();
                }
            });
        });
    });

    $$( '#socio-economic-button' ).off( 'click' ).on( 'click', function( event ) {
        window.localStorage.setItem('social_economic_producer_id', $$('#producer-id').val() );

    });

});

$$( document ).on( 'page:init', '.page[data-page="socio-economic"]', function( event ) {

    $$( 'li#hide_show_village, li#hide_show_amount, li#hide_show_stove, li#hide_show_stove, li#hide_show_organiser,'+
        'li#hide_show_venue, li#hide_show_about' ).hide();

    $$( '#belong_to_village' ).change( function( event ) {
        $$( this ).prop( 'checked' ) === true ? $$( 'li#hide_show_village' ).show() : $$( 'li#hide_show_village' ).hide();
    });
    $$( '#income_increase' ).change( function( event ) {
        $$( this ).prop( 'checked' ) === true ? $$( 'li#hide_show_amount' ).show() : $$( 'li#hide_show_amount' ).hide();
    });
    $$( '#attend_workshop' ).change( function( event ) {
        $$( this ).prop( 'checked' ) === true ? $$( 'li#hide_show_organiser, li#hide_show_venue, li#hide_show_about' ).show() : $$( 'li#hide_show_organiser, li#hide_show_venue, li#hide_show_about' ).hide();
    });
    $$( '#has_improved_stove' ).change( function( event ) {
        $$( this ).prop( 'checked' ) === true ? $$( 'li#hide_show_stove' ).show() : $$( 'li#hide_show_stove' ).hide();
    });

    $$( '#save-social-economic' ).off( 'click' ).on( 'click', function( event ) {

        var crops_grown = $$( '#crops-grown' ) ,
            crop_yield_before_project = $$( '#crop-yield-before-project' ) ,
            current_crop_yield = $$( '#current-crop-yield' ) ,
            other_enterprises = $$( '#other-enterprises' ),
            funding_sources = $$( '#funding-sources' ),
            seedling_sources = $$( '#seedling-sources' ),
            carbon_income_use = $$( '#carbon-income-use' ),
            belong_to_village = $$( '#belong_to_village').prop( 'checked' ) === true ? 1 : 2 ,
            attend_workshop = $$( '#attend_workshop').prop( 'checked' ) === true ? 1 : 2 ,
            income_increase = $$( '#income_increase').prop( 'checked' ) === true ? 1 : 2 ,
            has_improved_stove = $$( '#has_improved_stove').prop( 'checked' ) === true ? 1 : 2 ,
            village_group_name = $$( '#village-group-name'),
            organizer = $$( '#organizer'),
            where_was_it = $$( '#where-was-it'),
            was_about = $$( '#was-about'),
            income_increase_amount = $$( '#income_increase_amount'),
            stove_provider = $$( '#stove_provider');

        db.transaction( function( tx ) {
            tx.executeSql( 'INSERT INTO social_economics ( producer_id , year , crops_grown  , crop_yield_before  , current_crop_yield  ,other_enterprises  , fund_source  , carbon_income  , belong_to_group   , village_group_name  ,   attend_training  , training_organiser  , training_venue   , training_aim  ,income_crease , income_increase_amount  , has_improved_stove , stove_provider  , created_at  , updated_at  ) VALUES ( ?, ?, ? , ? , ? , ? , ? , ?, ? , ? ,?, ? , ? , ? , ? , ? , ?, ? , ? ,? )',
                [ localStorage.getItem('social_economic_producer_id')  , window.localStorage.getItem('next_year') , crops_grown.val() , crop_yield_before_project.val() ,  current_crop_yield.val() , other_enterprises.val() , funding_sources.val() , carbon_income_use.val() , belong_to_village , village_group_name.val() , attend_workshop, organizer.val() , where_was_it.val() , was_about.val(), income_increase , income_increase_amount.val() ,has_improved_stove , stove_provider.val() , get_current_timestamp() ,get_current_timestamp()  ], function( tx, result ) {
                    $$( '.back' ).click();

                    loadSocioEconomic();
                });

        });

    });
});

function uploadData( table ) {
    upload_in_progress = true;

    switch( table ) {

        case 'subcounties':
            db.transaction(
                function( tx ) {

                    tx.executeSql( 'SELECT sub_counties.id AS sId, sub_counties.name AS sName, sub_counties.district_id AS sDid, districts.remote_id AS dRid, districts.id AS dId FROM sub_counties '+
                        ' LEFT JOIN districts ON districts.id = sub_counties.district_id WHERE sub_counties.remote_id IS NULL LIMIT ?,?',
                        [( ( uploadPagination.page - 1 ) * uploadPagination.per_page ), uploadPagination.per_page ],
                        function ( tx, result ) {
                            var subcounties = [];

                            for ( i = 0; i < result.rows.length; i++ ) {

                                subcounties.push({
                                    id: result.rows.item( i ).sId,
                                    name: result.rows.item( i ).sName,
                                    district: result.rows.item( i ).dRid,
                                });
                            }

                            if ( subcounties.length > 0 ) {

                                $$.post( apiBase + 'inbound', {
                                        method: 'post_sub_counties',
                                        data: subcounties,
                                        token: window.localStorage.getItem( 'token' ),
                                        account: currentUser.remote_id
                                    },
                                    function( response ) {

                                        var response = JSON.parse( response );

                                        if ( response.result == 'error' )
                                            app.alert( response.message );

                                        else {
                                            $$.each( response.pairs, function ( index, value ) {
                                                db.transaction( function( tx ) {
                                                        tx.executeSql( 'UPDATE sub_counties SET remote_id = ? WHERE id = ? ',
                                                            [ value.remote_id, value.id ], null,null
                                                        );
                                                    },
                                                    null
                                                );
                                            });

                                            if ( subcounties.length < uploadPagination.per_page ) {
                                                subcounties_uploaded = true;
                                                upload_in_progress = false;

                                            }

                                        }

                                    },
                                    function() {
                                        uploadData( 'subcounties' );
                                    }
                                );
                            }

                            else {

                                subcounties_uploaded = true;
                                upload_in_progress = false;
                                uploadPagination.page = 1;

                                uploadData( 'villages' );
                            }
                        }, null
                    );
                },
                null
            );

            break;
        case 'villages':
            db.transaction(
                function( tx ) {
                    tx.executeSql( 'SELECT villages.id AS vId, villages.name AS vName, sub_counties.remote_id AS sRid FROM villages '+
                        ' LEFT JOIN sub_counties ON sub_counties.id = villages.sub_county_id WHERE villages.remote_id IS NULL LIMIT ?,?',
                        [( ( uploadPagination.page - 1 ) * uploadPagination.per_page ), uploadPagination.per_page ],
                        function ( tx, result ) {
                            var villages = [];

                            for ( i = 0; i < result.rows.length; i++ ) {

                                villages.push({
                                    id: result.rows.item( i ).vId,
                                    name: result.rows.item( i ).vName,
                                    sub_county: result.rows.item( i ).sRid,
                                });
                            }

                            if ( villages.length > 0 ) {

                                $$.post( apiBase + 'inbound', {
                                        method: 'post_villages',
                                        data: villages,
                                        token: window.localStorage.getItem( 'token' ),
                                        account: currentUser.remote_id
                                    },
                                    function( response ) {

                                        var response = JSON.parse( response );

                                        if ( response.result == 'error' )
                                            app.alert( response.message );

                                        else {

                                            $$.each( response.pairs, function ( index, value ) {
                                                db.transaction( function( tx ) {
                                                        tx.executeSql( 'UPDATE villages SET remote_id = ? WHERE id = ? ',
                                                            [ value.remote_id, value.id ], null,null
                                                        );
                                                    },
                                                    null
                                                );
                                            });

                                            if ( villages.length < uploadPagination.per_page ) {
                                                villages_uploaded = true;
                                                upload_in_progress = false;

                                            }

                                        }

                                    },
                                    function() {
                                        uploadData( 'villages');
                                    }
                                );
                            }

                            else {
                                villages_uploaded = true;
                                upload_in_progress = false;
                                uploadPagination.page = 1;

                                uploadData( 'producer_groups' );

                            }
                        }, null
                    );
                },
                null
            );

            break;
        case 'producer_groups':
            db.transaction(
                function( tx ) {
                    tx.executeSql( 'SELECT producer_groups.id AS pGid, producer_groups.name AS pGname, villages.remote_id AS vRid FROM producer_groups '+
                        ' LEFT JOIN villages ON villages.id = producer_groups.village_id WHERE producer_groups.remote_id IS NULL LIMIT ?,?',
                        [( ( uploadPagination.page - 1 ) * uploadPagination.per_page ), uploadPagination.per_page ],
                        function ( tx, result ) {
                            var producer_groups = [];

                            for ( i = 0; i < result.rows.length; i++ ) {

                                producer_groups.push({
                                    id: result.rows.item( i ).pGid,
                                    name: result.rows.item( i ).pGname,
                                    village: result.rows.item( i ).vRid,
                                    account: currentUser.remote_id
                                });
                            }

                            if ( producer_groups.length > 0 ) {

                                $$.post( apiBase + 'inbound', {
                                        method: 'post_producer_groups',
                                        data: producer_groups,
                                        token: window.localStorage.getItem( 'token' ),
                                        account: currentUser.remote_id
                                    },
                                    function( response ) {

                                        var response = JSON.parse( response );

                                        if ( response.result == 'error' )
                                            app.alert( response.message );

                                        else {

                                            $$.each( response.pairs, function ( index, value ) {
                                                db.transaction( function( tx ) {
                                                        tx.executeSql( 'UPDATE producer_groups SET remote_id = ? ,updated_at = ? WHERE id = ? ',
                                                            [ value.remote_id, get_current_timestamp(), value.id ], null,null
                                                        );
                                                    },
                                                    null
                                                );
                                            });

                                            if ( producer_groups.length < uploadPagination.per_page ) {
                                                producer_groups_uploaded = true;
                                                upload_in_progress = false;

                                            }
                                            else
                                                uploadData( 'producer_groups' );

                                        }

                                    },
                                    function() {
                                        uploadData( 'producer_groups' );
                                    }
                                );
                            }

                            else {
                                producer_groups_uploaded = true;
                                upload_in_progress = false;
                                uploadPagination.page = 1;

                                uploadData( 'producers' );
                            }
                        }, null
                    );
                },
                null,
                null
            );

            break;
        case 'producers' :

            //app.alert( 'Starting producers' );

            db.transaction(
                function( tx ) {
                    tx.executeSql( 'SELECT producers.id AS pId, first_name AS first, other_names AS others, date_of_birth AS dob, phone_number AS phone,  bank_details, gender, producer_groups , village_id, villages.remote_id AS vRid FROM producers LEFT JOIN '+
                        'villages ON villages.id = producers.village_id WHERE producers.remote_id IS NULL LIMIT ?,?',
                        [( ( uploadPagination.page - 1 ) * uploadPagination.per_page ), uploadPagination.per_page ],
                        function ( tx, result ) {
                            var producers = [];

                            for ( i = 0; i < result.rows.length; i++ ) {
								let bank_info = JSON.parse( result.rows.item( i ).bank_details );
								let groups = JSON.parse( result.rows.item( i ).producer_groups );
                                
                                producers.push({
                                    id: result.rows.item( i ).pId,
                                    first: result.rows.item( i ).first,
                                    others: result.rows.item( i ).others,
                                    village: result.rows.item( i ).vRid,
                                    gender: result.rows.item( i ).gender,
                                    dob: result.rows.item( i ).dob,
                                    phone: result.rows.item( i ).phone,
                                    bank_info: bank_info,
                                    group: groups,
                                    avatar: result.rows.item( i ).photo

                                });
                            }

                            if ( producers.length > 0 ) {

                                $$.post( apiBase + 'inbound', {
                                        method: 'post_producers',
                                        data: producers,
                                        token: window.localStorage.getItem( 'token' ),
                                        account: currentUser.remote_id
                                    },
                                    function( response ) {

                                        var response = JSON.parse( response );

                                        if ( response.result == 'error' )
                                            app.alert( response.message );

                                        else {
                                            db.transaction( function( tx ) {

                                                $$.each( response.pairs, function ( index, value ){

                                                    tx.executeSql( 'UPDATE producers SET remote_id = ?, code = ? ,updated_at = ?  WHERE id = ? ', [ value.remote_id, value.code, get_current_timestamp(), value.id, ],
                                                        function( tx,result) {

                                                            if( index == ( response.pairs.length - 1) ) {

                                                                if ( producers.length < uploadPagination.per_page ) {
                                                                    producers_uploaded = true;
                                                                    upload_in_progress = false;

                                                                }

                                                                //app.alert( 'Producers uploaded via update tx ' );
                                                            }

                                                        }, null
                                                    );

                                                });
                                            });

                                        }

                                    },
                                    function() {
                                        uploadData( 'producers' );

                                    });
                            }
                            else {
                                producers_uploaded = true;
                                upload_in_progress = false;
                                uploadPagination.page = 1;

                                uploadData( 'subcounties' );
                                //	uploadData( 'areas' );
                                //app.alert( 'Producers done via no records' );
                            }
                        },
                        function(tx, error ) {

                            app.alert( error.message );

                        }
                    );
                },
                null,
                null
            );

            break;
        default:
            uploadData( 'subcounties' );
    }

}

$$( '.technician-name' ).text( currentUser.remote_id );

document.addEventListener( 'deviceready', function() {

    $$( 'a#logout' ).on( 'click', function () {
        window.localStorage.removeItem( 'currentUser' );
        app.closePanel();
        openLoginScreen();
    });

    if ( window.localStorage.getItem( 'databaseSetup' ) == null )
        createDatabaseTables();

    if ( window.localStorage.getItem( 'installationRegistered' ) == null )
        registerInstallation();

    else {
        if ( window.localStorage.getItem( 'setOneSetup' ) == null )
            downloadData( 'countries' );

        else {
            if ( window.localStorage.getItem( 'currentUser' ) != null ) {
                currentUser = JSON.parse( window.localStorage.getItem( 'currentUser' ) );

                if ( window.localStorage.getItem( 'technicianSetup' ) == null )
                    setupTechnician();
            }

            else
                openLoginScreen();
        }
    }

    if ( window.localStorage.getItem( "technicianSetup" ) == 1 )
        uploadData( 'subcounties' );

}, false );

document.addEventListener( 'backbutton', function() {
    $$( '.back' ).click();
}, false);
